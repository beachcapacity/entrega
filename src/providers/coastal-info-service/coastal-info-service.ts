import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';

export interface CoastalInfoInterface {token: string; client: string; }

@Injectable()
export class CoastalInfoService {

    constructor(public http: HTTP) {
    
    }

    private getRequestOptions(type) {
      return { 'Content-Type':  type , 'api_key' : 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0aGVycmVyYUBpbnZlbnRpYXBsdXMuY29tIiwianRpIjoiNmRkYTUxMmMtZDIzNC00ZmQxLWI2MTAtNDI0YTA1MzllNmM0IiwiaXNzIjoiQUVNRVQiLCJpYXQiOjE1OTE3MDkxMDEsInVzZXJJZCI6IjZkZGE1MTJjLWQyMzQtNGZkMS1iNjEwLTQyNGEwNTM5ZTZjNCIsInJvbGUiOiIifQ.DxavOf07drwBZusTPnuuAlEOQChGTzYt3yuCVI7kQ_8'};
    }

    callService(method, url, head, type): Promise<any> {
      return new Promise<any>((resolve, reject) => {
        const headers = this.getRequestOptions(type);
        if(method == 'GET'){
          this.http.get(url, {}, head?headers:{})
            .then(response => {              
               const  body = response.data;
              if(body){
                console.log("RESOLVED");
                resolve(body);
              }else{
                reject("Error: respuesta sin contenido");
              }
            }, (error)  => {
              console.error('ERROR - api call, status: ' + JSON.stringify(error));
              reject(error);
            }).catch(e=>{
              console.log(e)}
            );
        }else{
            // Method isn't get and it's wrong
        }
      });
    }

}
