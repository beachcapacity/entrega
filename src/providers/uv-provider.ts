import {Injectable} from '@angular/core';
import { HTTP } from '@ionic-native/http';
import 'rxjs/Rx';

const URL = 'http://api.openweathermap.org/data/2.5/uvi';
const API_KEY = '97b2b2f8ff00ba360f957c769e0e4dc0'

@Injectable()
export class UVProvider {

    loading: any;

    constructor (
        private http: HTTP,
    ) {}

    call(lat, lon) {
        let parameters = {
            'appid': API_KEY,
            'lat': lat,
            'lon': lon 
        };
        let headers = {'Accept': 'application/json'};
        return this.http.get(URL, parameters, headers);
    }
}