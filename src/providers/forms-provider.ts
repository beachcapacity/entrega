import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_URL} from './config'
import 'rxjs/Rx';

import { UserProvider, User } from './user-provider';

let getMotivosURL = SERVER_URL+"api/core/flag/reason";
let getPeligrosURL = SERVER_URL+"api/core/flag/dangerous";
let getSustanciasURL = SERVER_URL+"api/core/incident/substances";
let getAlarmantesURL = SERVER_URL+"api/core/incident/ending/alarmsender";
let getMediosURL = SERVER_URL+"api/core/incident/ending/resourcesused";
let getExternosURL = SERVER_URL+"api/core/incident/ending/externalresourcesused";
let getMunicipiosURL = SERVER_URL+"api/core/municipalities";
let getPaisesURL = "https://restcountries.com/v3.1/all"; //External API that must be kept up to date or crash the Login service. Very important
let getPerfilesURL = SERVER_URL+"api/core/incident/profiles";
let getTrasladosURL = SERVER_URL+"api/core/incident/ending/translationcenter";
let getMotivosAuxilioURL = SERVER_URL+"api/core/incident/firstaid/type";
let getCausasAuxilioURL = SERVER_URL+"api/core/incident/firstaid/cause";
let getMotivosRescateURL = SERVER_URL+"api/core/incident/rescue";
let getMotivosEvacuacionURL = SERVER_URL+"api/core/incident/incidentevacuation";
let getCuerposURL = SERVER_URL+"api/core/mobilizedagents";

let getGeneralConfigURL = SERVER_URL+"api/core/generalconfig";

@Injectable()
export class FormsProvider {

    motivos: any[] = [];
    peligros: any[] = [];
    sustancias: any[] = [];
    alarmantes: any[] = [];
    medios: any[] = [];
    externos: any[] = [];
    municipios: any[] = [];
    paises: any[] = [];
    perfiles: any[] = [];
    traslados: any[] = [];
    motivosAuxilio: any[] = [];
    causasAuxilio: any[] = [];
    motivosRescate: any[] = [];
    motivosEvacuacion: any[] = [];
    cuerpos: any[] = [];

    generalConfig: any[] = [];

    user: User;

    paisesFrecuentes: string[] = [
        'Francia',
        'Bélgica',
        'Irlanda',
        'Italia',
        'Finlandia',
        'Holanda',
        'Dinamarca',
        'Noruega',
        'Suecia',
        'Alemania',
        'Reino Unido',
        'España'
    ];

    constructor (
        private _http: Http,
        private userProvider: UserProvider,
    ) {
        this.user = this.userProvider.getUser();
    }

    // Motivos Bandera
    getMotivosData():Promise<boolean | void> {
        console.log("Getmotivosdata");
        return new Promise((resolve, reject) => {
            this.getMotivosFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadMotivos();
                        if (this.motivos.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadMotivos();
                    if (this.motivos.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getMotivosFromServer() {
        console.log("Getmotivosfromserver");
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getMotivosURL+'?access_token='+this.user.access_token, options)
            .map(res => this.motivosToArray(res.json()))
            .catch(this.handleError);
    }

    motivosToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.motivos = [];
            for(let item of json) {
                this.motivos.push(item.name);
            }
            this.saveMotivos();
            return true;
        }
    }

    saveMotivos() {
        localStorage.setItem('motivos', JSON.stringify(this.motivos));
    }

    loadMotivos() {
        this.motivos = localStorage.getItem('motivos') ? JSON.parse(localStorage.getItem('motivos')) : [];
    }

    getMotivos() {
        return this.motivos;
    }

    // Peligros Bandera
    getPeligrosData():Promise<boolean |void > {
        return new Promise((resolve, reject) => {
            this.getPeligrosFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadPeligros();
                        if (this.peligros.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadPeligros();
                    if (this.peligros.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getPeligrosFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getPeligrosURL+'?access_token='+this.user.access_token, options)
            .map(res => this.peligrosToArray(res.json()))
            .catch(this.handleError);
    }

    peligrosToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.peligros = [];
            for(let item of json) {
                this.peligros.push(item.name);
            }
            this.savePeligros();
            return true;
        }
    }

    savePeligros() {
        localStorage.setItem('peligros', JSON.stringify(this.peligros));
    }

    loadPeligros() {
        this.peligros = localStorage.getItem('peligros') ? JSON.parse(localStorage.getItem('peligros')) : [];
    }

    getPeligros() {
        return this.peligros;
    }

    // Sustancias Incidente Persona
    getSustanciasData():Promise<boolean |void > {
        return new Promise((resolve, reject) => {
            this.getSustanciasFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadSustancias();
                        if (this.sustancias.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadSustancias();
                    if (this.sustancias.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getSustanciasFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getSustanciasURL+'?access_token='+this.user.access_token, options)
            .map(res => this.sustanciasToArray(res.json()))
            .catch(this.handleError);
    }

    sustanciasToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.sustancias = [];
            for(let item of json) {
                this.sustancias.push(item);
            }
            this.saveSustancias();
            return true;
        }
    }

    sustanciasFormValue(ids) {
        let result = [];
        if (ids) {
            ids.forEach(id => {
                result.push({id: id, name: this.sustanciasName(id)})
            });
        }
        return result;
    }

    sustanciasName(id) {
        let result = 'Otro';
        this.sustancias.forEach(item => {
            if (item.id == id) {
                result = item.name;
            }
        });

        return result;
    }

    saveSustancias() {
        localStorage.setItem('sustancias', JSON.stringify(this.sustancias));
    }

    loadSustancias() {
        this.peligros = localStorage.getItem('sustancias') ? JSON.parse(localStorage.getItem('sustancias')) : [];
    }

    getSustancias() {
        return this.sustancias;
    }

    // Alarmantes Incidente
    getAlarmantesData():Promise<boolean |void> {
        return new Promise((resolve, reject) => {
            this.getAlarmantesFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadAlarmantes();
                        if (this.alarmantes.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadAlarmantes();
                    if (this.alarmantes.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getAlarmantesFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getAlarmantesURL+'?access_token='+this.user.access_token, options)
            .map(res => this.alarmantesToArray(res.json()))
            .catch(this.handleError);
    }

    alarmantesToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.alarmantes = [];
            for(let item of json) {
                this.alarmantes.push(item.name);
            }
            this.saveAlarmantes();
            return true;
        }
    }

    saveAlarmantes() {
        localStorage.setItem('alarmantes', JSON.stringify(this.alarmantes));
    }

    loadAlarmantes() {
        this.alarmantes = localStorage.getItem('alarmantes') ? JSON.parse(localStorage.getItem('alarmantes')) : [];
    }

    getAlarmantes() {
        return this.alarmantes;
    }

    // Medios Incidente
    getMediosData():Promise<boolean |void > {
        return new Promise((resolve, reject) => {
            this.getMediosFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadMedios();
                        if (this.medios.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadMedios();
                    if (this.medios.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getMediosFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getMediosURL+'?access_token='+this.user.access_token, options)
            .map(res => this.mediosToArray(res.json()))
            .catch(this.handleError);
    }

    mediosToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.medios = [];
            for(let item of json) {
                this.medios.push(item);
            }
            this.saveMedios();
            return true;
        }
    }

    mediosFormValue(ids) {
        let result = [];
        if (ids) {
            ids.forEach(id => {
                result.push({id: id, name: this.mediosName(id)})
            });
        }
        return result;
    }

    mediosName(id) {
        let result = 'Otro';
        this.medios.forEach(item => {
            if (item.id == id) {
                result = item.name;
            }
        });

        return result;
    }

    saveMedios() {
        localStorage.setItem('medios', JSON.stringify(this.medios));
    }

    loadMedios() {
        this.medios = localStorage.getItem('medios') ? JSON.parse(localStorage.getItem('medios')) : [];
    }

    getMedios() {
        return this.medios;
    }

    // Medios Externos Incidente
    getExternosData():Promise<boolean |void > {
        return new Promise((resolve, reject) => {
            this.getExternosFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadExternos();
                        if (this.externos.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadExternos();
                    if (this.externos.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getExternosFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getExternosURL+'?access_token='+this.user.access_token, options)
            .map(res => this.externosToArray(res.json()))
            .catch(this.handleError);
    }

    externosToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.externos = [];
            for(let item of json) {
                this.externos.push(item);
            }
            this.saveExternos();
            return true;
        }
    }

    externosFormValue(ids) {
        let result = [];
        if (ids) {
            ids.forEach(id => {
                result.push({id: id, name: this.externosName(id)})
            });
        }
        return result;
    }

    externosName(id) {
        let result = 'Otro';
        this.externos.forEach(item => {
            if (item.id == id) {
                result = item.name;
            }
        });

        return result;
    }

    saveExternos() {
        localStorage.setItem('externos', JSON.stringify(this.externos));
    }

    loadExternos() {
        this.externos = localStorage.getItem('externos') ? JSON.parse(localStorage.getItem('externos')) : [];
    }

    getExternos() {
        return this.externos;
    }

    // Municipios Incidente Persona
    getMunicipiosData():Promise<boolean |void> {
        return new Promise((resolve, reject) => {
            this.getMunicipiosFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadMunicipios();
                        if (this.municipios.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadMunicipios();
                    if (this.municipios.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getMunicipiosFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getMunicipiosURL+'?access_token='+this.user.access_token, options)
            .map(res => this.municipiosToArray(res.json()))
            .catch(this.handleError);
    }

    municipiosToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.municipios = [];
            for(let item of json) {
                this.municipios.push(item.name);
            }
            this.saveMunicipios();
            return true;
        }
    }

    saveMunicipios() {
        localStorage.setItem('municipios', JSON.stringify(this.municipios));
    }

    loadMunicipios() {
        this.municipios = localStorage.getItem('municipios') ? JSON.parse(localStorage.getItem('municipios')) : [];
    }

    getMunicipios() {
        return this.municipios;
    }

    // Paises
    getPaisesData():Promise<boolean |void > {
        return new Promise((resolve, reject) => {
            this.getPaisesFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadPaises();
                        if (this.paises.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadPaises();
                    if (this.paises.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getPaisesFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getPaisesURL, options)
            .map(res => this.paisesToArray(res.json()))
            .catch(this.handleError);
    }

    paisesToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.paises = [];
            for(let item of json) {
                if (item.translations.es && this.paisesFrecuentes.indexOf(item.translations.es) === -1) {
                    this.paises.push(item.translations.es);
                }
            }
            this.paises.sort();
            for (let pais of this.paisesFrecuentes)
                this.paises.unshift(pais);
            this.savePaises();
            return true;
        }
    }

    savePaises() {
        localStorage.setItem('paises', JSON.stringify(this.paises));
    }

    loadPaises() {
        this.paises = localStorage.getItem('paises') ? JSON.parse(localStorage.getItem('paises')) : [];
    }

    getPaises() {
        return this.paises;
    }

    // Perfiles Incidente Persona
    getPerfilesData():Promise<boolean |void> {
        return new Promise((resolve, reject) => {
            this.getPerfilesFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadPerfiles();
                        if (this.perfiles.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadPerfiles();
                    if (this.perfiles.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getPerfilesFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getPerfilesURL+'?access_token='+this.user.access_token, options)
            .map(res => this.perfilesToArray(res.json()))
            .catch(this.handleError);
    }

    perfilesToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.perfiles = [];
            for(let item of json) {
                this.perfiles.push(item.name);
            }
            this.savePerfiles();
            return true;
        }
    }

    savePerfiles() {
        localStorage.setItem('perfiles', JSON.stringify(this.perfiles));
    }

    loadPerfiles() {
        this.perfiles = localStorage.getItem('perfiles') ? JSON.parse(localStorage.getItem('perfiles')) : [];
    }

    getPerfiles() {
        return this.perfiles;
    }

    // Traslados Incidente Persona
    getTrasladosData():Promise<boolean |void> {
        return new Promise((resolve, reject) => {
            this.getTrasladosFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadTraslados();
                        if (this.traslados.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadTraslados();
                    if (this.traslados.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getTrasladosFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getTrasladosURL+'?access_token='+this.user.access_token, options)
            .map(res => this.trasladosToArray(res.json()))
            .catch(this.handleError);
    }

    trasladosToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.traslados = [];
            for(let item of json) {
                this.traslados.push(item.name);
            }
            this.saveTraslados();
            return true;
        }
    }

    saveTraslados() {
        localStorage.setItem('traslados', JSON.stringify(this.traslados));
    }

    loadTraslados() {
        this.traslados = localStorage.getItem('traslados') ? JSON.parse(localStorage.getItem('traslados')) : [];
    }

    getTraslados() {
        return this.traslados;
    }

    // MotivosAuxilio
    getMotivosAuxilioData():Promise<boolean |void> {
        return new Promise((resolve, reject) => {
            this.getMotivosAuxilioFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadMotivosAuxilio();
                        if (this.motivosAuxilio.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadMotivosAuxilio();
                    if (this.motivosAuxilio.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getMotivosAuxilioFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getMotivosAuxilioURL+'?access_token='+this.user.access_token, options)
            .map(res => this.motivosAuxilioToArray(res.json()))
            .catch(this.handleError);
    }

    motivosAuxilioToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.motivosAuxilio = [];
            for(let item of json) {
                this.motivosAuxilio.push(item);
            }
            this.saveMotivosAuxilio();
            return true;
        }
    }

    motivosAuxilioFormValue(ids) {
        let result = [];
        if (ids) {
            ids.forEach(id => {
                result.push({id: id, name: this.motivosAuxilioName(id)})
            });
        }
        return result;
    }

    motivosAuxilioName(id) {
        let result = 'Otro';
        this.motivosAuxilio.forEach(item => {
            if (item.id == id) {
                result = item.name;
            }
        });

        return result;
    }

    saveMotivosAuxilio() {
        localStorage.setItem('motivosAuxilio', JSON.stringify(this.motivosAuxilio));
    }

    loadMotivosAuxilio() {
        this.motivosAuxilio = localStorage.getItem('motivosAuxilio') ? JSON.parse(localStorage.getItem('motivosAuxilio')) : [];
    }

    getMotivosAuxilio() {
        return this.motivosAuxilio;
    }

    // CausasAuxilio
    getCausasAuxilioData():Promise<boolean |void> {
        return new Promise((resolve, reject) => {
            this.getCausasAuxilioFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadCausasAuxilio();
                        if (this.causasAuxilio.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadCausasAuxilio();
                    if (this.causasAuxilio.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getCausasAuxilioFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getCausasAuxilioURL+'?access_token='+this.user.access_token, options)
            .map(res => this.causasAuxilioToArray(res.json()))
            .catch(this.handleError);
    }

    causasAuxilioToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.causasAuxilio = [];
            for(let item of json) {
                this.causasAuxilio.push(item);
            }
            this.saveCausasAuxilio();
            return true;
        }
    }

    causasAuxilioFormValue(ids) {
        let result = [];
        if (ids) {
            ids.forEach(id => {
                result.push({id: id, name: this.causasAuxilioName(id)})
            });
        }
        return result;
    }

    causasAuxilioName(id) {
        let result = 'Otro';
        this.causasAuxilio.forEach(item => {
            if (item.id == id) {
                result = item.name;
            }
        });

        return result;
    }

    saveCausasAuxilio() {
        localStorage.setItem('causasAuxilio', JSON.stringify(this.causasAuxilio));
    }

    loadCausasAuxilio() {
        this.causasAuxilio = localStorage.getItem('causasAuxilio') ? JSON.parse(localStorage.getItem('causasAuxilio')) : [];
    }

    getCausasAuxilio() {
        return this.causasAuxilio;
    }

    // MotivosRescate
    getMotivosRescateData():Promise<boolean |void> {
        return new Promise((resolve, reject) => {
            this.getMotivosRescateFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadMotivosRescate();
                        if (this.motivosRescate.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadMotivosRescate();
                    if (this.motivosRescate.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getMotivosRescateFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getMotivosRescateURL+'?access_token='+this.user.access_token, options)
            .map(res => this.motivosRescateToArray(res.json()))
            .catch(this.handleError);
    }

    motivosRescateToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.motivosRescate = [];
            for(let item of json) {
                this.motivosRescate.push(item);
            }
            this.saveMotivosRescate();
            return true;
        }
    }

    motivosRescateFormValue(ids) {
        let result = [];
        if (ids) {
            ids.forEach(id => {
                result.push({id: id, name: this.motivosRescateName(id)})
            });
        }
        return result;
    }

    motivosRescateName(id) {
        let result = 'Otro';
        this.motivosRescate.forEach(item => {
            if (item.id == id) {
                result = item.name;
            }
        });

        return result;
    }

    saveMotivosRescate() {
        localStorage.setItem('motivosRescate', JSON.stringify(this.motivosRescate));
    }

    loadMotivosRescate() {
        this.motivosRescate = localStorage.getItem('motivosRescate') ? JSON.parse(localStorage.getItem('motivosRescate')) : [];
    }

    getMotivosRescate() {
        return this.motivosRescate;
    }

    // MotivosEvacuacion
    getMotivosEvacuacionData():Promise<boolean|void> {
        return new Promise((resolve, reject) => {
            this.getMotivosEvacuacionFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadMotivosEvacuacion();
                        if (this.motivosEvacuacion.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadMotivosEvacuacion();
                    if (this.motivosEvacuacion.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getMotivosEvacuacionFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getMotivosEvacuacionURL+'?access_token='+this.user.access_token, options)
            .map(res => this.motivosEvacuacionToArray(res.json()))
            .catch(this.handleError);
    }

    motivosEvacuacionToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.motivosEvacuacion = [];
            for(let item of json) {
                this.motivosEvacuacion.push(item.name);
            }
            this.saveMotivosEvacuacion();
            return true;
        }
    }

    saveMotivosEvacuacion() {
        localStorage.setItem('motivosEvacuacion', JSON.stringify(this.motivosEvacuacion));
    }

    loadMotivosEvacuacion() {
        this.motivosEvacuacion = localStorage.getItem('motivosEvacuacion') ? JSON.parse(localStorage.getItem('motivosEvacuacion')) : [];
    }

    getMotivosEvacuacion() {
        return this.motivosEvacuacion;
    }

    // Cuerpos
    getCuerposData():Promise<boolean |void> {
        return new Promise((resolve, reject) => {
            this.getCuerposFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadCuerpos();
                        if (this.cuerpos.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadCuerpos();
                    if (this.cuerpos.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getCuerposFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getCuerposURL+'?access_token='+this.user.access_token, options)
            .map(res => this.cuerposToArray(res.json()))
            .catch(this.handleError);
    }

    cuerposToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.cuerpos = [];
            for(let item of json) {
                this.cuerpos.push(item);
            }
            this.saveCuerpos();
            return true;
        }
    }

    cuerposFormValue(ids) {
        let result = [];
        if (ids) {
            ids.forEach(id => {
                result.push({id: id, name: this.cuerposName(id)})
            });
        }
        return result;
    }

    cuerposName(id) {
        let result = 'Otro';
        this.cuerpos.forEach(item => {
            if (item.id == id) {
                result = item.name;
            }
        });

        return result;
    }

    saveCuerpos() {
        localStorage.setItem('cuerpos', JSON.stringify(this.cuerpos));
    }

    loadCuerpos() {
        this.cuerpos = localStorage.getItem('cuerpos') ? JSON.parse(localStorage.getItem('cuerpos')) : [];
    }

    getCuerpos() {
        return this.cuerpos;
    }

    // GeneralConfig
    getGeneralConfigData():Promise<boolean |void> {
        return new Promise((resolve, reject) => {
            this.getGeneralConfigFromServer().subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        this.loadGeneralConfig();
                        if (this.generalConfig.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadGeneralConfig();
                    if (this.generalConfig.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getGeneralConfigFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getGeneralConfigURL+'?access_token='+this.user.access_token, options)
            .map(res => this.generalConfigToArray(res.json()))
            .catch(this.handleError);
    }

    generalConfigToArray(json) {
        if (json.error) {
            return false;
        } else {
            this.generalConfig = [];
            for(let item of json) {
                this.generalConfig[item.name] = item.value;
            }
            this.saveGeneralConfig();
            return true;
        }
    }

    saveGeneralConfig() {
        localStorage.setItem('generalConfig', JSON.stringify(this.generalConfig));
    }

    loadGeneralConfig() {
        this.motivosEvacuacion = localStorage.getItem('generalConfig') ? JSON.parse(localStorage.getItem('generalConfig')) : [];
    }

    getGeneralConfig() {
        return this.generalConfig;
    }

    // Error
    handleError(error) {
        console.log(error);
        return Observable.throw(error || 'Server error');
    }
}