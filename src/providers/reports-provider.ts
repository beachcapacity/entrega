import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import { HTTP } from '@ionic-native/http';
import {Observable} from 'rxjs/Observable';
import { DatePipe } from '@angular/common';
import {SERVER_URL} from './config'
import 'rxjs/Rx';

import { UserProvider, User } from './user-provider';

let flagReportURL = SERVER_URL+"api/action/flag";
let peopleReportURL = SERVER_URL+"api/action/influx";
let incidentReportURL = SERVER_URL+"api/action/incident";

export class FlagReport{
    id?: number;
    flag?: number;
    adviser?: number;
    beach?: number;
    date?: Date;
    dateRevision?: Date;
    dateArriado?: Date;
    flagExtra?: number;
    observation?: string;
    reason?: string;
    dangerReason?: string;
    temperature?: string;
    windOrientation?: string;
    windSpeed?: string;
    point?: string;
    sync: boolean = false;
}

export class PeopleReport{
    id?: number;
    lvl?: number;
    adviser?: number;
    beach?: number;
    temperature?: string;
    windOrientation?: string;
    windSpeed?: string;
    point?: string;
    date?: Date;
    sync: boolean = false;
}

export class IncidentReport{
    id?: number;
    adviser?: number;
    beach?: number;
    affectedPeopleNumber: number = 0;
    affectedPeople: AffectedPeople[] = [];
    dateEnd?: Date;
    dateIni?: Date;
    delayArriveExternalResources?: number;
    evacuation?: string;
    externalResourcesUsed?: string[];
    isLifeGuardCall?: number;
    hourCall112?: Date;
    cecoes?: string;
    image1?: string;
    image2?: string;
    image3?: string;
    mobilizedAgents?: string[];
    numberLifeGuard?: number;
    observation?: string;
    resourcesUsed?: string[];
    alarmSender?: string;
    type?: number;
    point?: string;
    lat?: number;
    lng?: number;
    sync: boolean = false;
}

export class AffectedPeople{
    age?: number;
    country?: string;
    emergencyGrade?: string;
    firstAidCauses?: string[];
    firstAidReason?: string[];
    gender?: string;
    highVolunteer?: boolean;
    municipality?: string;
    profile?: string;
    requireFirstAid?: boolean;
    rescueReason?: string[];
    substance?: string[];
    translationCenter?: string;
    typeIntervention?: number;
    usedDesa?: boolean;
    valid: boolean = false;
}

@Injectable()
export class ReportsProvider {

    loading: any;

    flagReports: FlagReport[] = [];
    peopleReports: PeopleReport[] = [];
    incidentReports: IncidentReport[] = [];

    user: User;

    emergencyGrade = {
        0: 'Ileso',
        1: 'Leve',
        2: 'Grave',
        3: 'Éxitus'
    }

    typeIntervention = {
        0: 'Primeros Auxilios',
        1: 'Rescate Acuático'
    }

    constructor (
        private _http: Http,
        private http: HTTP,
        private userProvider: UserProvider,
        public datepipe: DatePipe
    ) {}

    // Storage
    loadReports() {
        let flagReportsTemp = localStorage.getItem('flagReports') ? JSON.parse(localStorage.getItem('flagReports')) : [];
        for (let report of flagReportsTemp) {
            report.date = new Date(report.date);
            report.dateArriado = report.dateArriado ? new Date(report.dateArriado) : null;
            report.dateRevision = report.dateRevision ? new Date(report.dateRevision) : null;

            let diferencia = new Date().getTime() - report.date.getTime();
            if (!report.sync || diferencia < (1000*60*60*24*2)) {
                this.flagReports.push(report);
            }
        }
        let peopleReportsTemp = localStorage.getItem('peopleReports') ? JSON.parse(localStorage.getItem('peopleReports')) : [];
        for (let report of peopleReportsTemp) {
            report.date = new Date(report.date);

            let diferencia = new Date().getTime() - report.date.getTime();
            if (!report.sync || diferencia < (1000*60*60*24*2)) {
                this.peopleReports.push(report);
            }
        }
        let incidentReportsTemp = localStorage.getItem('incidentReports') ? JSON.parse(localStorage.getItem('incidentReports')) : [];
        for (let report of incidentReportsTemp) {
            report.dateIni = report.dateIni ? new Date(report.dateIni) : null;
            report.dateEnd = report.dateEnd ? new Date(report.dateEnd) : null;
            report.hourCall112 = report.hourCall112 ? new Date(report.hourCall112) : null;

            let diferencia = new Date().getTime() - report.dateIni.getTime();
            if (!report.sync || diferencia < (1000*60*60*24*2)) {
                this.incidentReports.push(report);
            }
        }
    }

    saveReports() {
        localStorage.setItem('flagReports', JSON.stringify(this.flagReports));
        localStorage.setItem('peopleReports', JSON.stringify(this.peopleReports));
        localStorage.setItem('incidentReports', JSON.stringify(this.incidentReports));
    }

    // Get/Set Reports
    newFlagReport(flagReport) {
        this.flagReports.push(flagReport);
        this.orderReports(this.flagReports);
    }

    getFlagReports() {
        return this.flagReports;
    }

    newPeopleReport(peopleReport) {
        this.peopleReports.unshift(peopleReport);
    }

    getPeopleReports() {
        return this.peopleReports;
    }

    newIncidentReport(incidentReport) {
        this.incidentReports.push(incidentReport);
        this.orderReports(this.incidentReports);
    }

    getIncidentReports() {
        return this.incidentReports;
    }

    //Order
    orderReports(reports) {
        reports.sort(function(a, b) {
            if (a.date > b.date) {
                return -1
            }
            if (a.date > b.date) {
                return 1
            }
            return 0;
        });
    }

    //Syncing
    sync(): Promise<boolean|void> {
        return new Promise((resolve, reject) => {
            this.sendFlagReports().then(res => {
                this.sendPeopleReports().then(res => {
                    this.sendIncidentReports().then(res => {
                        this.saveReports();
                        resolve();
                    },
                    error => {
                        return reject();
                    });
                },
                error => {
                    return reject();
                });
            },
            error => {
                return reject();
            });
        });
    }


    sendFlagReports(): Promise<boolean|void> {
        return new Promise((resolve, reject) => {
            if (this.flagReports.length == 0) {
                resolve();
            }
            for (let i = this.flagReports.length-1; i >= 0; i--) {
                if (this.flagReports[i].sync) {
                    if (i == 0) {
                        resolve();
                    }
                } else {
                    this.sendFlagReport(this.flagReports[i]).subscribe(
                        data => {
                        if (data.status == "OK") {
                            this.flagReports[i].id = data.id;
                            this.flagReports[i].sync = true;
                            if (i == 0) {
                                resolve();
                            }
                        } else {
                            return reject();
                        }
                        },
                        error => {
                        console.log(error);
                        return reject();
                        }
                    );
                }
            }
        });
    }

    sendFlagReport(flagReport: FlagReport) {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        let flagAction = JSON.stringify({
            id: flagReport.id,
            flag: flagReport.flag,
            adviser: flagReport.adviser,
            beach: flagReport.beach,
            date: this.datepipe.transform(flagReport.date, 'dd/LL/yyyy HH:mm'),
            dateRevision: flagReport.dateRevision ? this.datepipe.transform(flagReport.dateRevision, 'dd/LL/yyyy HH:mm') : null,
            dateArriado: flagReport.dateArriado ? this.datepipe.transform(flagReport.dateArriado, 'dd/LL/yyyy HH:mm') : null,
            flagExtra: flagReport.flagExtra,
            dangerReason: flagReport.dangerReason,
            observation: flagReport.observation,
            reason: flagReport.reason,
            temperature: flagReport.temperature,
            windOrientation: flagReport.windOrientation,
            windSpeed: flagReport.windSpeed,
            point: flagReport.point
        });
        console.log(flagAction);
        return this._http.post(flagReportURL+'?access_token='+this.userProvider.getUser().access_token+"&flagAction="+flagAction, 
            null, 
            options
        ).map(res => res.json()).catch(this.handleError);
    }

    sendPeopleReports(): Promise<boolean |void> {
        return new Promise((resolve, reject) => {
            if (this.peopleReports.length == 0) {
                resolve();
            }
            for (let i = this.peopleReports.length-1; i >= 0; i--) {
                if (this.peopleReports[i].sync) {
                    if (i == 0) {
                        resolve();
                    }
                } else {
                    this.sendPeopleReport(this.peopleReports[i]).subscribe(
                        data => {
                        if (data.status == "OK") {
                            this.peopleReports[i].id = data.id;
                            this.peopleReports[i].sync = true;
                            if (i == 0) {
                                resolve();
                            }
                        } else {
                            return reject();
                        }
                        },
                        error => {
                        console.log(error);
                        return reject();
                        }
                    );
                }
            }
        });
    }

    sendPeopleReport(peopleReport: PeopleReport) {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        let influxAction = JSON.stringify({
            id: peopleReport.id,
            lvl: peopleReport.lvl,
            adviser: peopleReport.adviser,
            beach: peopleReport.beach,
            temperature: peopleReport.temperature,
            windOrientation: peopleReport.windOrientation,
            windSpeed: peopleReport.windSpeed,
            point: peopleReport.point,
            date: this.datepipe.transform(peopleReport.date, 'dd/LL/yyyy HH:mm')
        });
        console.log(influxAction);
        return this._http.post(
            peopleReportURL+'?access_token='+this.userProvider.getUser().access_token+'&influxAction='+influxAction, 
            null,
            options
        ).map(res => res.json()).catch(this.handleError);
    }

    sendIncidentReports(): Promise<boolean |void> {
        console.log('sendIncidentReports');
        return new Promise((resolve, reject) => {
            if (this.incidentReports.length == 0) {
                resolve();
            }
            let i = 0;
            for (let report of this.incidentReports) {
                i++;
                if (report.sync) {
                    if (i == this.incidentReports.length) {
                        resolve();
                    }
                } else {
                    this.sendIncidentReport(report).then(
                        data => {
                            if (JSON.parse(data.data).status == "OK") {
                                report.id = JSON.parse(data.data).id;
                                report.sync = true;
                                if (i == this.incidentReports.length) {
                                    resolve();
                                }
                            } else {
                                console.log(data.error);
                                return reject();
                            }
                        },
                        error => {
                            console.log(error);
                            return reject();
                        }
                    );
                }
            }
        });
    }

    sendIncidentReport(incidentReport: IncidentReport) {
        //console.log(incidentReport);
        //let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        //let options = new RequestOptions({ headers: headers });
        let incidentAction = JSON.stringify({
            id: incidentReport.id,
            adviser: incidentReport.adviser,
            beach: incidentReport.beach,
            point: incidentReport.point,
            affectedPeopleNumber: incidentReport.affectedPeopleNumber,
            affectedPeople: this.parseAffectedPeople(incidentReport.affectedPeople),
            dateIni: this.datepipe.transform(incidentReport.dateIni, 'LL/dd/yyyy HH:mm'),
            dateEnd: incidentReport.dateEnd ? this.datepipe.transform(incidentReport.dateEnd, 'LL/dd/yyyy HH:mm') : null,
            delayArriveExternalResources: incidentReport.delayArriveExternalResources,
            evacuation: incidentReport.evacuation,
            externalResourcesUsed: incidentReport.externalResourcesUsed,
            isLifeGuardCall: incidentReport.isLifeGuardCall,
            hourCall112: incidentReport.hourCall112 ? this.datepipe.transform(incidentReport.hourCall112, 'LL/dd/yyyy HH:mm') : null,
            cecoes: incidentReport.cecoes,
            image1: incidentReport.image1,
            image2: incidentReport.image2,
            image3: incidentReport.image3,
            mobilizedAgents: incidentReport.mobilizedAgents,
            numberLifeGuard: incidentReport.numberLifeGuard,
            observation: incidentReport.observation,
            resourcesUsed: incidentReport.resourcesUsed,
            alarmSender: incidentReport.alarmSender,
            type: incidentReport.type
        });
        //console.log(incidentAction);
        this.http.setDataSerializer('urlencoded');
        return this.http.post(
            incidentReportURL+'?access_token='+this.userProvider.getUser().access_token, 
            {incidentAction: incidentAction},
            {
                'Accept': 'application/json',
            }
        );
    }

    parseAffectedPeople(affectedPeople: AffectedPeople[]) {
        console.log('parseAffected begin');
        let res = [];
        for (let ap of affectedPeople) {
            let item = {
                age: ap.age,
                country: ap.country,
                emergencyGrade: ap.emergencyGrade,
                firstAidCauses: ap.firstAidCauses,
                firstAidReason: ap.firstAidReason,
                gender: ap.gender,
                highVolunteer: ap.highVolunteer,
                municipality: ap.municipality,
                profile: ap.profile,
                requireFirstAid: ap.typeIntervention ? ap.requireFirstAid : true,
                rescueReason: ap.rescueReason,
                substance: ap.substance,
                translationCenter: ap.translationCenter,
                typeIntervention: this.typeIntervention[ap.typeIntervention],
                usedDesa: ap.usedDesa
            }
            res.push(item);
        }
        console.log('parseAffected end');
        return res;
    }

    arrayToReadable(array) {
        console.log('array to readable begin');
        if (!array) {
            return null;
        }
        let primero = true;
        let res = '';
        for(let item of array) {
            if (primero) {
                primero = false;
            } else {
                res = res + ', ';
            }
            res = res + item;
        }
        console.log('array to readable end');
        return res;
    }

    handleError(error) {
        console.log(error);
        return Observable.throw(error || 'Server error');
    }
}