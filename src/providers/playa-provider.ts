import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_URL} from './config'
import 'rxjs/Rx';

let playasURL = SERVER_URL+"core/beach";
let cercanasURL = SERVER_URL+"core/beach/nearest"; ///api/core/beach/nearest?lat=28.400891&lon=-16.674011

const limiteCercanas = 1000; // km
const sensibilidadClick = 500 // metros

export class Playa{
    id: number;
    name: string;
    DGSE: string;
    island: string;
    municipality: string;
    longitude: string;
    latitude: string;
    flag?: number;
    temperature?: number;
    windSpeed?: number;
    windOrientation?: string;
    influxLvl?: number;
    sector?: string;
    updated?: Date;
    aemet?: string;
    srArea?: number;
    isBlueFlag?: boolean;
    haveDisabilityAccess?: boolean;
    id_sanidad?: string;
    horaInicio?: number;
    horaFin?: number;
    municipalityEntity?: Municipality;
}

export class Municipality{
    id: number;
    island: string;
    name: string;
}

@Injectable()
export class PlayaProvider {

    loading: any;

    playas: Playa[] = [];
    cercanas: Playa[] = [];

    islas: any[] = [];
    municipios: any[] = [];
    municipiosName: any[] = [];

    constructor (
        private _http: Http,
    ) {}

    // Get
    getPlayasCercanas() {
        if (this.cercanas.length > 0) {
            console.log(this.cercanas);
            return this.cercanas;
        } else {
            console.log(this.playas);
            return this.playas;
        }
    }

    getPlayas() {
        return this.playas;
    }

    getIslas() {
        return this.islas;
    }

    getMunicipios() {
        return this.municipios;
    }

    // Todas las playas
    getPlayasData():Promise<boolean| void> {
        return new Promise((resolve, reject) => {
            this.getPlayasFromServer().subscribe(
                res => {
                    if (res) {
                        this.savePlayas();
                        resolve();
                    } else {
                        this.loadPlayas();
                        if (this.playas.length > 0) {
                            resolve();
                        } else {
                            reject();
                        }
                    }
                },
                error => {
                    this.loadPlayas();
                    if (this.playas.length > 0) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            );
        });
    }

    getPlayasFromServer() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(playasURL, options)
            .map(res => this.setPlayasData(res.json()))
            .catch(this.handleError);
    }

    setPlayasData(json) {
        if (json.error) {
            return false;
        } else {
            this.playas = [];
            for (let playa of json) {
                playa.sector = playa.DGSE%100;
                playa.updated = new Date;
                if (playa.sector) {
                    playa.name = playa.name + ' (S: ' + playa.sector + ')';
                }
                this.playas.push(playa);
                if (this.islas.indexOf(playa.island) === -1 && playa.island) { 
                    this.islas.push(playa.island);
                }
                if (this.municipiosName.indexOf(playa.municipality) === -1 && playa.municipality) { 
                    this.municipiosName.push(playa.municipality);
                    this.municipios.push({name: playa.municipality, island: playa.island});
                }
            }
            return true;
        }
    }

    // Playas Cercanas
    getCercanasData(lat: number, lon: number):Promise<boolean|void > {
        return new Promise((resolve, reject) => {
            this.getCercanasFromServer(lat, lon).subscribe(
                res => {
                    if (res) {
                        resolve();
                    } else {
                        reject();
                    }
                },
                error => {
                    reject();
                }
            );
        });
    }

    getCercanasFromServer(lat: number, lon: number) {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(cercanasURL+'?lat='+lat+'&lon='+lon, options)
            .map(res => this.setCercanasData(res.json()))
            .catch(this.handleError);
    }

    setCercanasData(json) {
        if (json.error) {
            return false;
        } else {
            this.cercanas = [];
            for (let playa of json) {
                if (playa.meters < limiteCercanas*1000) {
                    playa.data.sector = playa.data.DGSE%100;
                    playa.data.updated = new Date;
                    if (playa.data.sector) {
                        playa.data.name = playa.data.name + ' (S: ' + playa.data.sector + ')';
                    }
                    this.cercanas.push(playa.data);
                }
            }
            return true;
        }
    }

    // Save and Load
    loadPlayas() {
        this.islas = localStorage.getItem('islas') ? JSON.parse(localStorage.getItem('islas')) : [];
        this.municipios = localStorage.getItem('municipios') ? JSON.parse(localStorage.getItem('municipios')) : [];
        this.playas = localStorage.getItem('playas') ? JSON.parse(localStorage.getItem('playas')) : [];
        for (let playa of this.playas) {
            playa.updated = new Date(playa.updated);
        }
    }

    savePlayas() {
        localStorage.setItem('playas', JSON.stringify(this.playas));
        localStorage.setItem('islas', JSON.stringify(this.islas));
        localStorage.setItem('municipios', JSON.stringify(this.municipios));
    }

    // Get Nearest to coordinates
    getNearestToCoords(coords) {
        let cercanas = [];
        for(let playa of this.playas) {
            let metros = this.getDistanceFromLatLon(coords[1], coords[0], playa.latitude, playa.longitude)
            if (metros < sensibilidadClick) {
                cercanas.push({playa: playa, metros: metros});
            }
        }
        if (cercanas.length > 0) {
            cercanas.sort((a, b) => a.metros < b.metros ? -1 : a.metros> b.metros ? 1 : 0);
            console.log(cercanas);
            return cercanas[0].playa;
        } else {
            return false;
        }
    }

    getDistanceFromLatLon(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
        var dLon = this.deg2rad(lon2-lon1); 
        var a = 
          Math.sin(dLat/2) * Math.sin(dLat/2) +
          Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
          Math.sin(dLon/2) * Math.sin(dLon/2)
          ; 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c; // Distance in km
        d = d * 1000 // Distance in meters
        return d;
      }
      
      deg2rad(deg) {
        return deg * (Math.PI/180)
      }

    handleError(error) {
        return Observable.throw(error || 'Server error');
    }
}