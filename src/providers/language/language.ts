import { Injectable } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { Globalization } from '@ionic-native/globalization';

/*
  Generated class for the LanguageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LanguageProvider {

  private defaultLanguage: string = 'es';
  private currentLanguageStorageKey = 'infoplayas.current_language';
  private currentLanguage: string;
  private languageHasBeenSet: boolean = false;

  private availableLanguages = [
    {
      'code': 'es',
      'name': 'SPANISH',
    },
    {
      'code': 'en',
      'name': 'ENGLISH',
    },
    {
      'code': 'de',
      'name': 'GERMAN',
    },
    {
      'code': 'fr',
      'name': 'FRENCH',
    },
    {
      'code': 'eusk',
      'name': 'EUSKERA',
    },
    {
      'code': 'gall',
      'name': 'GALLEGO',
    },
    {
      'code': 'cat',
      'name': 'CATALAN',
    },
  ];

  constructor(
    private translateService: TranslateService,
    private globalization: Globalization,
  ) {
    // TODO: Set language only if not has been set yet
    this.currentLanguage = window.localStorage[this.currentLanguageStorageKey] ? window.localStorage[this.currentLanguageStorageKey] : this.defaultLanguage;

    // Try to set already selected language
    this.translateService.setDefaultLang(this.defaultLanguage);
    if (this.languageHasBeenSet && this.getSuitableLanguage(this.currentLanguage) !== this.currentLanguage) {
      this.translateService.use(this.currentLanguage);
    } else {
    }
  }

  public setLanguageFromBrowser(): Promise<any> {
    if ((<any>window).cordova) {
      return this.globalization.getPreferredLanguage().then(result => {
        let language = this.getSuitableLanguage(result.value) || this.defaultLanguage;
        this.translateService.use(language);
      });
    } else {
      let browserLanguage = this.translateService.getBrowserLang() || this.defaultLanguage;
      let language = this.getSuitableLanguage(browserLanguage);
      this.translateService.use(language);

      return Promise.resolve();
    }
  }

  public setLanguage(language: string): Promise<any> {
    return this.translateService.use(this.getSuitableLanguage(language)).toPromise();
  }

  private getSuitableLanguage(language: string): string {
    let trimmedLanguage = language.substring(0, 2).toLowerCase();
    return this.availableLanguages.some(x => x.code == trimmedLanguage) ? trimmedLanguage : this.defaultLanguage;
  }

}
