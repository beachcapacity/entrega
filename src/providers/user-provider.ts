import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {SERVER_URL} from './config'
import 'rxjs/Rx';
import { Playa } from './playa-provider';

let loginURL = SERVER_URL+"oauth/v2/token";
let getUserDataURL = SERVER_URL+"api/core/user/data";

export class User{

    id?: number;
    access_token?: string;
    expires_in?: number;
    token_type?: string;
    refresh_token?: string;
    username?: string;
    error?: string;
    error_description?: string;
    roles?: string;
    token?: string;
    email?: string;
    date?: Date;
    sector?: string;
    dgse?: string;
    playa?: Playa;
    municipalityEntity?: Municipality[];
    manager: boolean = false;
    online: boolean = false;
}

export class Municipality{
    id: number;
    island: string;
    name: string;
}

@Injectable()
export class UserProvider {

    loading: any;

    user: User = new User;
    municipios: Municipality[];
    constructor (
        private _http: Http,
    ) {}

    private formData(myFormData){
        return Object.keys(myFormData).map(function(key){
            return encodeURIComponent(key) + '=' + encodeURIComponent(myFormData[key]);
        }).join('&');
    }

    getUser() {
        return this.user;
    }

    login(request) {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.post(loginURL, this.formData(request), options)
            .map(res => this.jsonToUser(res.json()))
            .catch(this.handleError);
    }

    jsonToUser(json) {
        if (json.error) {
            return false;
        } else {
            this.user = json;
            return true;
        }
    }

    logout() {
        this.user.online = false;
    }

    getUserData() {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this._http.get(getUserDataURL+'?access_token='+this.user.access_token, options)
            .map(res => this.setUserData(res.json()))
            .catch(this.handleError);
    }

    setUserData(json) {
        console.log(json);
        if (json.error) {
            return false;
        } else {
            this.user.id = json.id;
            this.user.username = json.username;
            this.user.roles = json.roles;
            this.user.token = json.token;
            this.user.email = json.email;
            this.user.manager = json.manager;
            this.user.municipalityEntity = json.municipalities;
            return true;
        }
    }

    saveUser() {
        localStorage.setItem('user', JSON.stringify(this.user));
        this.user.online = true;
    }

    loadUser() {
        this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : new User();
    }

    setLocation(date, playa, sector, dgse) {
        this.user.date = date;
        this.user.playa = playa;
        this.user.sector = sector;
        this.user.dgse = dgse;
    }

    handleError(error) {
        return Observable.throw(error.json().error_description || 'Server error');
    }
    getMunicipios() {
        return this.user.municipalityEntity;
    }
}