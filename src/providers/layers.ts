import { Injectable } from '@angular/core';
import { layer, proj, source } from "openlayers";

export interface CustomLayer {
  id: string,
  name: string,
  layer?: layer.Tile,
  visible?: boolean;
}

// Production Maps Key
const bingMapsKey: string = 'AkuDwBOq5kCO5i7DpQW5bCpbOOdLA0EuIuWNr0kLSDNLOeejdCfwBn7yGL6xZ4tn';

@Injectable()
export class LayersProvider {

  private _baseLayers: CustomLayer[] = [];
  private currentBaseLayer: number = 1;

  constructor() {
    this._baseLayers.push({
      id: 'bing',
      name: 'Mapa Base Bing',
      layer: new layer.Tile({
        visible: false,
        source: new source.BingMaps({
          key: bingMapsKey,
          imagerySet: 'Aerial', // https://docs.microsoft.com/en-us/bingmaps/rest-services/imagery/get-a-static-map#map-parameters
        }),
      }),
    });

    this._baseLayers.push({
      id: 'grafcan',
      name: 'Mapa Base GrafCan',
      layer: new layer.Tile({
        visible: true,
        source: new source.TileWMS({
          url: 'http://idecan3.grafcan.es/ServicioWMS/OrtoExpress_bat?',
          params: {
            'VERSION': '1.1.1',
            'LAYERS': 'WMS_OrtoExpress',
            'FORMAT': 'image/jpeg',
            'TRANSPARENT': true,
            'CRS': 'EPSG:32628',
          },
          projection: proj.get('EPSG:32628'),
          crossOrigin: 'anonymous',
        }),
      }),
    });
  }

  get baseLayers(): CustomLayer[] {
    return this._baseLayers;
  }

  get baseLayer(): CustomLayer {
    return this._baseLayers[this.currentBaseLayer];
  }

}