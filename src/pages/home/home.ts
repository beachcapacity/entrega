import { Component } from '@angular/core';
import { NavController, LoadingController, Loading } from 'ionic-angular';
import { PublicListPage } from '../public-list/public-list';
import { PublicMapPage } from '../public-map/public-map';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loading: Loading;

  publicList = PublicListPage;
  publicMap = PublicMapPage;

  constructor(
    public navCtrl: NavController,
    public loadingCrtl: LoadingController
  ) {}

  goToLogin() {
    this.navCtrl.push(LoginPage);
  }

}
