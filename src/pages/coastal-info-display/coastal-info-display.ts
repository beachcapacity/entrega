import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {CoastalInfoService} from "../../providers/coastal-info-service/coastal-info-service";


  
@Component({
  selector: 'coastal-info-display',
  templateUrl: 'coastal-info-display.html'
})


export class CoastalInfoDisplayPage {

    parsedPage; // El html de la página de la que extraemos la información
    tideArray;  // El dictionary-array donde metemos todas las predicciones 
    index: string[] = [];      // Array de index para el diccionario

ionViewDidLoad(){ 
  /*let auxDate = new Date();
  let year = auxDate.getFullYear();
  let month = auxDate.getMonth()+1;
  let day = auxDate.getDate();*/
  this.getApiInfo("https://ideihm.covam.es/api-ihm/getmarea?request=gettide&amp;id=57&amp;format=txt" );
}


  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public cis: CoastalInfoService,
              ) {
  }

  // Parsea la página de html y con regex extraemos toda la información que necesitamos. 
  scrapeData() {
    let tides = [];
    let index = [];
    var week = new Array(7);
    week[0] = "sun";
    week[1] = "mon";
    week[2] = "tue";
    week[3] = "wed";
    week[4] = "thu";
    week[5] = "fri";
    week[6] = "sat";

    let elementPatt = new RegExp('((<pre class="predictions-table">)[^]*(<\/pre>))', "g");
    let supposedlyData = this.parsedPage.match(elementPatt)[0].split('\n');    

    supposedlyData.forEach(function(data){
      try { 
      let timePatt = new RegExp('[0-9]{1,2}:[0-9]{2}[^]{1,2}[A|P]{1}M{1}',"g");
      let yearPatt = new RegExp('[0-9]{4}-[0-9]{2}-[0-9]{2}',"g");
      let tideMetersPatt = new RegExp('[0-6]{1}\.[0-9]{2}[^]{1,2}(meters)',"g");
      let tidePatt = new RegExp('(Low Tide)|(High Tide)',"g");
      let sunPatt = new RegExp('(Moonset)|(Sunset)|(Sunrise)|(Moonrise)',"g");

      if(data.match(timePatt)) {

        let timeOG = data.match(timePatt).toString();
        
        // Convertimos el tiempo de 12h+PM/AM a 24h.
        const [time2, modifier] = timeOG.split(' ');
        let [hours, minutes] = time2.split(':');
        if (hours === '12' && modifier === 'PM') {
          hours = '00';
        } 
        if (modifier === 'AM' && hours.length < 2) {
          hours = '0'+ hours;
        }
        if (modifier === 'PM') {
          hours = parseInt(hours, 10) + 12;
        }
        
        let time = `${hours}:${minutes}`;
        let year = data.match(yearPatt);
        let date = new Date(year+"T"+time);
        let weekday = week[date.getDay()];
        let day = year.toString().substr(8,2);
        let month = year.toString().substr(5,2);
        let tideMeters = data.match(tideMetersPatt);
        tideMeters = tideMeters ? tideMeters.toString().replace("meters","") : null;
        let tide = data.match(tidePatt);
        tide = tide ? tide.toString().toLowerCase().replace(' ','_') : null;
        let sun = data.match(sunPatt);
        sun = sun ? sun.toString().toLowerCase() : null;

        let i = ''+year[0];

        if (!tides[i]) {
          tides[i] = {
            'day'  :  null,
            'month': null,
            'year' : null,
            'weekday' : null,
            'date' : null,
            'tide' : [],
            'sun' : []
          };
        }

        tides[i].day = day;
        tides[i].month = month;
        tides[i].year = year.toString().substr(0,4);
        tides[i].weekday = weekday;
        tides[i].date = date;

        if (tide) {
          tides[i].tide[tide] = {
            'tideMeters': tideMeters,
            'time': time
          };
        }

        if (sun) {
          tides[i].sun[sun] = {
            'time': time
          };
        }

        if (index.indexOf(i) === -1) {
          index.push(i);
        }

      } else {
      }} catch(e) {
        console.log(e);
      }
    });
    this.tideArray = tides; 
    this.index = index; 
    console.log(this.tideArray);
    console.log(this.index);
  }

  back() {
    this.navCtrl.pop();
  }

  getApiInfo(url) {
    // callservice -> REST, url, headers(bool), type
    this.cis.callService('GET', url, false,'text/html').then((e) => {
      this.parsedPage = e;
      this.scrapeData();
    }).catch(e=> {
      console.log("Error in getApiUrl");
      console.log(e);
    });
    }
}
