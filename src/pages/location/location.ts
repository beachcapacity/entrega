import { User, UserProvider, Municipality } from './../../providers/user-provider';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading, ModalController, MenuController, AlertController, Footer } from 'ionic-angular';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Playa, PlayaProvider } from '../../providers/playa-provider';
import { IonicSelectableComponent } from 'ionic-selectable';

import { TabsPage } from '../tabs/tabs';
import { HomePage } from '../home/home';
import { FormsProvider } from '../../providers/forms-provider';

@Component({
  selector: 'page-location',
  templateUrl: 'location.html'
})

export class LocationPage {

  now: Date = new Date;

  locationForm: FormGroup; 
  date: AbstractControl;
  playa: AbstractControl;
  sector: AbstractControl;
  dgse: AbstractControl;
  municipality: AbstractControl;


  playas: Playa[];
  municipalities:Municipality[];
  playasFull: Playa[] = [];
  municipalitiesFull: Municipality[] = [];

  loading: Loading;
  back() {
    this.navCtrl.push(HomePage);
  }
  constructor(
    private fb: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public toastCrtl: ToastController, 
    public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    private userProvider: UserProvider,
    private playaProvider: PlayaProvider,
    private alertCtrl: AlertController,
    private loadingController: LoadingController,
    private formsProvider: FormsProvider
  ) {

/* Creating a form group with the following fields: date, playa, sector, dgse, and municipality. */
    this.locationForm = this.fb.group({
      'date': [this.now.toISOString(), Validators.compose([Validators.required])],
      'playa': ['', Validators.compose([Validators.required])],
      'sector': ['', Validators.compose([Validators.required])],
      'dgse': ['', Validators.compose([Validators.required])],
      'municipality': ['', Validators.compose([Validators.required])],
    });

/* Creating a reference to the form controls. */
    this.date = this.locationForm.controls['date'];
    this.playa = this.locationForm.controls['playa'];
    this.sector = this.locationForm.controls['sector'];
    this.dgse = this.locationForm.controls['dgse'];
    this.municipality = this.locationForm.controls['municipality'];
  }

  ionViewWillEnter() {
    
 /* Creating a copy of the original array. */
    this.playasFull = this.playaProvider.getPlayasCercanas();
    this.playas = [...this.playasFull];
    this.municipalitiesFull = this.userProvider.getMunicipios();
    this.municipalities = [...this.municipalitiesFull];

/* Setting the values of the form controls to the first item in the array. */
    this.playa.setValue(this.playas['']);
    this.dgse.setValue(this.playas[0].DGSE);
    this.sector.setValue(this.playas[0].sector);
    this.municipality.setValue(this.municipalities[0]);

/* Filtering the playas array based on the municipalityEntity.id and the haveAccess function. */
    if (this.userProvider.user.municipalityEntity.length) {
      this.playas = this.playas.filter((item) => {

        if (this.municipality.value.id==item.municipalityEntity.id){
        return (item.municipalityEntity.id && this.haveAccess(item.municipalityEntity.id));}
      });
    
    }




  }

  async ionViewDidLoad() {
/**
 * The function returns a promise that resolves or rejects after a delay.
 * @param value - The value to resolve the promise with.
 * @param delay - The time in milliseconds to wait before resolving or rejecting the promise.
 * @returns A promise that will resolve or reject after a delay.
 */
 function resolveTimeout(value, delay) {
  return new Promise(
    resolve => setTimeout(() => resolve(value), delay)
  );
}
function rejectTimeout(reason, delay) {
  return new Promise(
    (r, reject) => setTimeout(() => reject(reason), delay)
  );
}


 /* Creating a bunch of promises. */
      const task0 = this.formsProvider.getMotivosData();
      const task1 = this.formsProvider.getPeligrosData();
      const task2 = this.formsProvider.getAlarmantesData();
      const task3 = this.formsProvider.getMediosData();
      const task4 = this.formsProvider.getExternosData();
      const task5 = this.formsProvider.getMotivosEvacuacionData()
      const task6 = this.formsProvider.getCuerposData();
      const task7 = this.formsProvider.getSustanciasData();
      const task8 = this.formsProvider.getPerfilesData();
      const task9 = this.formsProvider.getMotivosRescateData();
      const task10 =this.formsProvider.getMotivosAuxilioData()
      const task11 =this.formsProvider.getTrasladosData();
      const task12 =this.formsProvider.getMunicipiosData();
      const task13 =this.formsProvider.getPaisesData();
      const task14 =this.formsProvider.getGeneralConfigData();
      const task15 = this.formsProvider.getCausasAuxilioData(); 

      /* Trying to get data from a server. */
      const GetData = Promise.all(
        [resolveTimeout([task0,task1,task2, task3,task4,task5,task6,task7,task8,task9,task10,task11,task12,task13,task14,task15],1000), 
         rejectTimeout(new Error('Debe conectarse a internet al menos una vez'), 1000)]);
      
      try{
        const Data = await GetData;
      }catch(error){ 
        this.showError(error.message)}
      
    


    
   

    // keeping this legacy promise chain in case something goes wrong
   // this.formsProvider.getMotivosData().then(
      //res => {
        //this.formsProvider.getPeligrosData().then(
          //res => {
            //this.formsProvider.getAlarmantesData().then(
              //res => {
               // this.formsProvider.getMediosData().then(
                 // res => {
                   // this.formsProvider.getExternosData().then(
                     // res => {
                       // this.formsProvider.getMotivosEvacuacionData().then(
                         // res => {
                            //this.formsProvider.getCuerposData().then(
                              //res => {
                                //this.formsProvider.getSustanciasData().then(
                                  //res => {
                                    //this.formsProvider.getPerfilesData().then(
                                      //res => {
                                        //this.formsProvider.getMotivosRescateData().then(
                                          //res => {
                                           // this.formsProvider.getMotivosAuxilioData().then(
                                             // res => {
                                                //this.formsProvider.getCausasAuxilioData().then(
                                                  //res => {
                                                    //this.formsProvider.getTrasladosData().then(
                                                      //res => {
                                                       // this.formsProvider.getMunicipiosData().then(
                                                         // res => {
                                                           // this.formsProvider.getPaisesData().then(
                                                             // res => {
                                                                //this.formsProvider.getGeneralConfigData().then(res => {
                                                                  //this.loading.dismiss();},error => 
                                                                  //{this.showError("Debe conectarse a internet al menos una vez");});
                                                              //},
                                                              //error => {
                                                               // this.showError("Debe conectarse a internet al menos una vez");
                                                             // }
                                                           // );
                                                          //},
                                                         // error => {
                                                        //    this.showError("Debe conectarse a internet al menos una vez");
                                                      //    }
                                                    //    );
                                                      //},
                                                      //error => {
                                                        //this.showError("Debe conectarse a internet al menos una vez");
                                                      //}
                                                    //);
                                                  //},
                                                  //error => {
                                                   // this.showError("Debe conectarse a internet al menos una vez");
                                                 // }
                                               // );
                                              //},
                                             // error => {
                                               // this.showError("Debe conectarse a internet al menos una vez");
                                             // }
                                           // );
                                          //},
                                          //error => {
                                           // this.showError("Debe conectarse a internet al menos una vez");
                                         // }
                                       // );
                                      //},
                                      //error => {
                                       // this.showError("Debe conectarse a internet al menos una vez");
                                     // }
                                   // );
                                  //},
                                  //error => {
                                   // this.showError("Debe conectarse a internet al menos una vez");
                                 // }
                               // );
                              //},
                              //error => {
                             //   this.showError("Debe conectarse a internet al menos una vez");
                           //   }
                         //   );
                          //},
                          //error => {
                           // this.showError("Debe conectarse a internet al menos una vez");
                         // }
                       // );
                      //},
                      //error => {
                        //this.showError("Debe conectarse a internet al menos una vez");
                      //}
                    //);
                  //},
                  //error => {
                    //this.showError("Debe conectarse a internet al menos una vez");
                  //}
                //);
              //},
             // error => {
               // this.showError("Debe conectarse a internet al menos una vez");
             // }
           // );
         // },
         // error => {
           // this.showError("Debe conectarse a internet al menos una vez");
         // }
       // );
     // },
     // error => {
       // this.showError("Debe conectarse a internet al menos una vez");
      //}
   // );
  }

/**
 * If the user has selected a municipality, then filter the beaches to only show those that are in the
 * selected municipality.
 */
  onchangeMunicipality(){
    this.playas = [...this.playasFull];
    
    if (this.userProvider.user.municipalityEntity.length) {
      this.playas = this.playas.filter((item) => {

        if (this.municipality.value.id==item.municipalityEntity.id){
          this.playa.setValue("");
        return (item.municipalityEntity.id && this.haveAccess(item.municipalityEntity.id));}
        

      });
    
    }
  }
  
  
/**
 * I'm trying to set the user's location in the userProvider, and then navigate to the TabsPage.
 * @param value - {
 */
  setLocation(value) {

    if(this.locationForm.valid) {

      this.userProvider.setLocation(
        new Date(value.date),
        value.playa,
        value.sector,
        value.dgse
      );

      console.log(this.userProvider.getUser());
      this.navCtrl.setRoot(TabsPage);

    }

  }

/**
 * The function is called when the user selects an item from the dropdown list. 
 * 
 * The function then sets the value of the DGSE and sector form controls to the values of the DGSE and
 * sector properties of the selected item.
 * @param event - {
 */
  setDGSE(event: {

    

    component: IonicSelectableComponent,
    value: any 
  }){

    this.dgse.setValue(event.value.DGSE);
    this.sector.setValue(event.value.sector);
  }

/**
 * If the user has access to the beach, return true.
 * @param beachId - the id of the beach
 * @returns The result of the function.
 */
  haveAccess(beachId) {
    var result = false;
    this.userProvider.user.municipalityEntity.forEach(element => {
      if (element.id == beachId) {
        result = true;
      }
    });
    return result;
  }

/**
 * "If the user clicks the 'Cancel' button, the alert is dismissed and the function returns false. If
 * the user clicks the 'Accept' button, the user is logged out and the alert is dismissed, then the
 * user is redirected to the HomePage."
 * 
 * I hope this helps!</code>
 */
  logout() {
    let alert = this.alertCtrl.create({
      title: 'Cerrar sesión',
      message: '¿Está seguro que desea cerrar la sesión?',
      buttons: [
          {
              text: 'Cancelar',
              handler: () => {
                  alert.dismiss();
                  return false;
              }
          },
          {
              text: 'Aceptar',
              handler: () => {
                  this.userProvider.logout();
                  alert.dismiss().then(() => {
                    this.navCtrl.setRoot(HomePage);
                  });
              }
          }
      ]
    });
    alert.present();
  }

/**
 * It creates an alert with the title "Error" and the subTitle of the error message that was passed in.
 * @param errorMsg - The error message to display.
 */
  showError(errorMsg) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: errorMsg,
      buttons: ['OK']
    });
    alert.present();
  }

}
