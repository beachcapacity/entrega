import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { App } from 'ionic-angular';

import { FlagFormPage } from '../flag-form/flag-form';

import { ReportsProvider } from '../../providers/reports-provider';
import { User, UserProvider } from '../../providers/user-provider';
//import {Router} from '@angular/router';




@Component({
  selector: 'page-flag-reports',
  templateUrl: 'flag-reports.html'
})
export class FlagReportsPage {

  reports: any[] = [];
  syncNumber: number = 0;

  user: User;

  constructor(
    public navCtrl: NavController,
    private app: App,
    private reportsProvider: ReportsProvider,
    private userProvider: UserProvider,
  ) {
    this.user = this.userProvider.getUser();

    this.reports = this.reportsProvider.getFlagReports();
    this.countSyncs();
  }

  ngAfterViewChecked() {
    this.countSyncs();
  }

  newFlag() {
    this.app.getRootNav().push(FlagFormPage, {nuevo: true});
  }

  copyFlag(report) {
    this.app.getRootNav().push(FlagFormPage, {nuevo: true, flagReport: report});
  }

  editFlag(report) {
    this.app.getRootNav().push(FlagFormPage, {nuevo: false, flagReport: report});
  }

  countSyncs() {
    this.syncNumber = 0;
    for (let report of this.reports) {
      if (report.sync && report.beach == this.user.playa.id) {
        this.syncNumber++;
      }
    }
  }

  addZero(number) {
    if (number <= 9) {
      return '0'+number;
    } else {
      return number;
    }
  }}
