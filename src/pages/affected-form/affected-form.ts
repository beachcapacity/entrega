import { Component, /*NgZone*/ } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading, ModalController, MenuController, AlertController } from 'ionic-angular';
import { FormGroup, AbstractControl, FormBuilder, Validators, /*ValidationErrors*/ } from '@angular/forms';
import { AffectedPeople } from '../../providers/reports-provider';
import { FormsProvider } from '../../providers/forms-provider';

@Component({
  selector: 'page-affected-form',
  templateUrl: 'affected-form.html'
})

export class AffectedFormPage {

  loading: Loading;

  affectedForm: FormGroup; 

  gender: AbstractControl;
  age: AbstractControl;
  country: AbstractControl;
  residente: AbstractControl;
  municipality: AbstractControl;
  substance: AbstractControl;
  profile: AbstractControl;
  typeIntervention: AbstractControl;
  emergencyGrade: AbstractControl;
  translationCenter: AbstractControl;
  highVolunteer: AbstractControl;

  // Rescate acuático
  rescueReason: AbstractControl;
  requireFirstAid: AbstractControl;

  // Primeros auxilios
  firstAidReason: AbstractControl;
  firstAidCauses: AbstractControl;
  usedDesa: AbstractControl;

  sustancias: any[] = [];
  perfiles: any[] = [];
  motivosRescate: any[] = [];
  motivosAuxilio: any[] = [];
  causasAuxilio: any[] = [];
  traslados: any[] = [];
  municipios: any[] = [];
  paises: any[] = [];

  affectedPeople: AffectedPeople;
  index: number;
  situation: number;
  isResident: boolean = true;

  constructor(
    private fb: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCrtl: LoadingController, 
    public toastCrtl: ToastController, 
    public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    private alertCtrl: AlertController,
    private formsProvider: FormsProvider
    ) {
      
      this.affectedPeople = this.navParams.data.affectedPeople;
      this.index = this.navParams.data.index;
      this.situation = this.navParams.data.situation;

      this.affectedForm = this.fb.group({
        'gender': [this.affectedPeople.gender, Validators.compose([Validators.required])],
        'age': [this.affectedPeople.age, Validators.compose([Validators.required])],
        'country': [this.affectedPeople.country, Validators.compose([Validators.required])],
        'residente': [this.affectedPeople.municipality == "No residente" ? false : true],
        'municipality': [this.affectedPeople.municipality],
        'substance': [this.formsProvider.sustanciasFormValue(this.affectedPeople.substance)],
        'profile': [this.affectedPeople.profile, Validators.compose([Validators.required])],
        'typeIntervention': [this.affectedPeople.typeIntervention, Validators.compose([Validators.required])],
        'emergencyGrade': [this.affectedPeople.emergencyGrade, Validators.compose([Validators.required])],
        'translationCenter': [this.affectedPeople.translationCenter],
        'highVolunteer': [this.affectedPeople.highVolunteer],

        'rescueReason': [this.formsProvider.motivosRescateFormValue(this.affectedPeople.rescueReason)],
        'requireFirstAid': [this.affectedPeople.requireFirstAid],

        'firstAidReason': [this.formsProvider.motivosAuxilioFormValue(this.affectedPeople.firstAidReason)],
        'firstAidCauses': [this.formsProvider.causasAuxilioFormValue(this.affectedPeople.firstAidCauses)],
        'usedDesa': [this.affectedPeople.usedDesa],
      });

      this.gender = this.affectedForm.controls['gender'];
      this.age = this.affectedForm.controls['age'];
      this.country = this.affectedForm.controls['country'];
      this.residente = this.affectedForm.controls['residente'];
      this.municipality = this.affectedForm.controls['municipality'];
      this.substance = this.affectedForm.controls['substance'];
      this.profile = this.affectedForm.controls['profile'];
      this.typeIntervention = this.affectedForm.controls['typeIntervention'];
      this.emergencyGrade = this.affectedForm.controls['emergencyGrade'];
      this.translationCenter = this.affectedForm.controls['translationCenter'];
      this.highVolunteer = this.affectedForm.controls['highVolunteer'];

      this.rescueReason = this.affectedForm.controls['rescueReason'];
      this.requireFirstAid = this.affectedForm.controls['requireFirstAid'];

      this.firstAidReason = this.affectedForm.controls['firstAidReason'];
      this.firstAidCauses = this.affectedForm.controls['firstAidCauses'];
      this.usedDesa = this.affectedForm.controls['usedDesa'];


  }

  ionViewWillEnter() {
    this.sustancias = this.formsProvider.getSustancias();
    this.perfiles = this.formsProvider.getPerfiles();
    this.motivosRescate = this.formsProvider.getMotivosRescate();
    this.motivosAuxilio = this.formsProvider.getMotivosAuxilio();
    this.causasAuxilio = this.formsProvider.getCausasAuxilio();
    this.traslados = this.formsProvider.getTraslados();
    this.municipios = this.formsProvider.getMunicipios();
    this.paises = this.formsProvider.getPaises();   
    this.checkInputs();
  }

  back() {
    this.navCtrl.pop();
  }

  showError(errorMsg) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: errorMsg,
      buttons: ['OK']
    });
    alert.present();
  }

  changeGender(gender) {
    if (this.gender.value !== null) {
      document.getElementById('gender-'+this.gender.value).classList.remove('button-active');
    }
    document.getElementById('gender-'+gender).classList.add('button-active');
    this.gender.setValue(gender);
  }

  changeRequireFirstAids(require) {
    if (this.requireFirstAid.value !== null) {
      document.getElementById('require-'+this.requireFirstAid.value).classList.remove('button-active');
    }
    document.getElementById('require-'+require).classList.add('button-active');
    this.requireFirstAid.setValue(require);
    if (!require) {
      this.firstAidReason.setValue(null);
      this.firstAidCauses.setValue(null);
      this.firstAidReason.clearValidators();
      this.firstAidReason.updateValueAndValidity();
      this.firstAidCauses.clearValidators();
      this.firstAidCauses.updateValueAndValidity();
      this.usedDesa.clearValidators();
      this.usedDesa.updateValueAndValidity();
    } else {
      this.firstAidReason.setValidators([Validators.required]);
      this.firstAidReason.updateValueAndValidity();
      this.firstAidCauses.setValidators([Validators.required]);
      this.firstAidCauses.updateValueAndValidity();
      this.usedDesa.setValidators([Validators.required]);
      this.usedDesa.updateValueAndValidity();
    }
  }

  changeTypeIntervention(type) {
    if (this.typeIntervention.value !== null) {
      document.getElementById('typeInt-'+this.typeIntervention.value).classList.remove('button-active');
    }
    document.getElementById('typeInt-'+type).classList.add('button-active');

    this.typeIntervention.setValue(type);

    if (type === 1) {
      this.rescueReason.setValidators([Validators.required]);
      this.rescueReason.updateValueAndValidity();
      this.requireFirstAid.setValidators([Validators.required]);
      this.requireFirstAid.updateValueAndValidity();
      this.firstAidReason.clearValidators();
      this.firstAidReason.updateValueAndValidity();
      this.firstAidCauses.clearValidators();
      this.firstAidCauses.updateValueAndValidity();
      this.usedDesa.clearValidators();
      this.usedDesa.updateValueAndValidity();
    } else if (type === 0) {
      this.rescueReason.clearValidators();
      this.rescueReason.updateValueAndValidity();
      this.requireFirstAid.clearValidators();
      this.requireFirstAid.updateValueAndValidity();
      this.firstAidReason.setValidators([Validators.required]);
      this.firstAidReason.updateValueAndValidity();
      this.firstAidCauses.setValidators([Validators.required]);
      this.firstAidCauses.updateValueAndValidity();
      this.usedDesa.setValidators([Validators.required]);
      this.usedDesa.updateValueAndValidity();
    }
  }

  changeUsedDesa(desa) {
    if (this.usedDesa.value !== null) {
      document.getElementById('desa-'+this.usedDesa.value).classList.remove('button-active');
    }
    document.getElementById('desa-'+desa).classList.add('button-active');
    this.usedDesa.setValue(desa);
  }

  changeEmergencyGrade(grade) {
    if (this.emergencyGrade.value !== null) {
      document.getElementById('grade-'+this.emergencyGrade.value).classList.remove('button-active');
    }
    document.getElementById('grade-'+grade).classList.add('button-active');
    this.emergencyGrade.setValue(grade);
  }

  changeHighVolunteer(high) {
    if (this.highVolunteer.value !== null) {
      document.getElementById('high-'+this.highVolunteer.value).classList.remove('button-active');
    }
    document.getElementById('high-'+high).classList.add('button-active');
    this.highVolunteer.setValue(high);
  }

  changeResident() {
    this.isResident = this.residente.value;
    console.log("isResident: "+this.isResident+" checkbox value: "+this.residente.value);
    if (this.residente.value) {
      this.municipality.setValidators([Validators.required]);
      this.municipality.updateValueAndValidity();
      this.country.setValue(this.paises[0])
      
    } else {
      this.municipality.clearValidators();
      this.municipality.updateValueAndValidity();
    }
  }

  checkInputs() { 
    if (this.gender.value !== null) {
      this.changeGender(this.gender.value);
    }
    if (this.requireFirstAid.value !== null) {
      this.changeRequireFirstAids(this.requireFirstAid.value);
    }
    if (this.typeIntervention.value !== null) {
      this.changeTypeIntervention(this.typeIntervention.value);
    }
    if (this.usedDesa.value !== null) {
      this.changeUsedDesa(this.usedDesa.value);
    }
    if (this.emergencyGrade.value !== null) {
      this.changeEmergencyGrade(this.emergencyGrade.value);
    }
    if (this.highVolunteer.value !== null) {
      this.changeHighVolunteer(this.highVolunteer.value);
    }
    this.changeResident();

    if (this.situation > 0) {
      this.highVolunteer.setValidators([Validators.required]);
      this.highVolunteer.updateValueAndValidity();
    }
  }

  setAffectedPeople(value) {
    if(this.affectedForm.valid) {
      this.affectedPeople.gender = value.gender;
      this.affectedPeople.age = value.age;
      this.affectedPeople.country = value.country;
      this.affectedPeople.municipality = value.residente ? value.municipality : "No residente";
      this.affectedPeople.substance = [];
      if (value.substance) {
        for (let item of value.substance) {
          this.affectedPeople.substance.push(item.id);
        }
      }
      this.affectedPeople.profile = value.profile;
      this.affectedPeople.typeIntervention = value.typeIntervention;
      this.affectedPeople.rescueReason = [];
      if (value.rescueReason) {
        for (let item of value.rescueReason) {
          this.affectedPeople.rescueReason.push(item.id);
        }
      }
      this.affectedPeople.requireFirstAid = value.requireFirstAid;
      this.affectedPeople.firstAidReason = [];
      if (value.firstAidReason) {
        for (let item of value.firstAidReason) {
          this.affectedPeople.firstAidReason.push(item.id);
        }
      }
      this.affectedPeople.firstAidCauses = [];
      if (value.firstAidCauses) {
        for (let item of value.firstAidCauses) {
          this.affectedPeople.firstAidCauses.push(item.id);
        }
      }
      this.affectedPeople.usedDesa = value.usedDesa;
      this.affectedPeople.emergencyGrade = value.emergencyGrade;
      this.affectedPeople.translationCenter = value.translationCenter;
      this.affectedPeople.highVolunteer = value.highVolunteer;

      this.affectedPeople.valid = true;

      console.log(this.affectedPeople);

      this.back();
    }
  }

}
