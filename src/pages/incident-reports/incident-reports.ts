import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { App } from 'ionic-angular';

import { IncidentFormPage } from '../incident-form/incident-form';

import { ReportsProvider } from '../../providers/reports-provider';
import { User, UserProvider } from '../../providers/user-provider';

@Component({
  selector: 'page-incident-reports',
  templateUrl: 'incident-reports.html'
})
export class IncidentReportsPage {

  reports: any[] = []; 
  syncNumber: number = 0;

  user: User;

  constructor(
    public navCtrl: NavController,
    private app: App,
    private reportsProvider: ReportsProvider,
    private userProvider: UserProvider
  ) {
    this.user = this.userProvider.getUser();

    this.reports = this.reportsProvider.getIncidentReports();
    this.countSyncs();
  }

  ngAfterViewChecked() {
    this.countSyncs();
  }

  newIncident() {
    this.app.getRootNav().push(IncidentFormPage, {nuevo: true});
  }

  copyIncident(report) {
    this.app.getRootNav().push(IncidentFormPage, {nuevo: true, incidentReport: report});
  }

  editIncident(report) {
    this.app.getRootNav().push(IncidentFormPage, {nuevo: false, incidentReport: report});
  }

  countSyncs() {
    this.syncNumber = 0;
    for (let report of this.reports) {
      if (report.sync && report.beach == this.user.playa.id) {
        this.syncNumber++;
      }
    }
  }

  addZero(number) {
    if (number <= 9) {
      return '0'+number;
    } else {
      return number;
    }
  }

}
