import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { App } from 'ionic-angular';

import { PeopleFormPage } from '../people-form/people-form';

import { ReportsProvider } from '../../providers/reports-provider';
import { User, UserProvider } from '../../providers/user-provider';

@Component({
  selector: 'page-people-reports',
  templateUrl: 'people-reports.html'
})
export class PeopleReportsPage {

  reports: any[] = []; 
  syncNumber: number = 0;

  user: User;

  constructor(
    public navCtrl: NavController,
    private app: App,
    private reportsProvider: ReportsProvider,
    private userProvider: UserProvider
  ) {
    this.user = this.userProvider.getUser();

    this.reports = this.reportsProvider.getPeopleReports();
    this.countSyncs();
  }

  ngAfterViewChecked() {
    this.countSyncs();
  }

  newPeople() {
    this.app.getRootNav().push(PeopleFormPage, {nuevo: true});
  }

  copyPeople(report) {
    this.app.getRootNav().push(PeopleFormPage, {nuevo: true, peopleReport: report});
  }

  editPeople(report) {
    this.app.getRootNav().push(PeopleFormPage, {nuevo: false, peopleReport: report});
  }

  countSyncs() {
    this.syncNumber = 0;
    for (let report of this.reports) {
      if (report.sync && report.beach == this.user.playa.id) {
        this.syncNumber++;
      }
    }
  }

  addZero(number) {
    if (number <= 9) {
      return '0'+number;
    } else {
      return number;
    }
  }

}
