import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Platform } from 'ionic-angular';
import { ITUNES_URL, GPLAY_URL } from '../../providers/config';

@Component({
  selector: 'update-page',
  templateUrl: 'update.html',
})
export class UpdatePage {

  msg = 'Su versión de la app está desfasada. Es necesario que actualice la versión para que los informes que realice tengan el formato correcto. Disculpe las molestias.';

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
    public toastCtrl: ToastController,
    private _platform: Platform,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateAppPage');
  }

  openWeb () {
    let url = "";
    if(this._platform.is('ios')){
      url = ITUNES_URL;
    }else{
      url = GPLAY_URL;
    }
    window.open(url,'_system', 'location=yes');
  }

}