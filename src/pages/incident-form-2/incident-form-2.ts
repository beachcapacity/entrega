import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { FormGroup, AbstractControl, FormBuilder, Validators, /*ValidationErrors*/ } from '@angular/forms';
import { UserProvider, User } from '../../providers/user-provider';
import { ReportsProvider, IncidentReport, AffectedPeople } from '../../providers/reports-provider';

import { AffectedFormPage } from '../affected-form/affected-form';
import { FormsProvider } from '../../providers/forms-provider';

@Component({
  selector: 'page-incident-form-2',
  templateUrl: 'incident-form-2.html'
})

export class IncidentForm2Page {

  loading: Loading;

  incidentForm: FormGroup; 

  // Sit 0
  dateIni: AbstractControl;
  affectedPeopleNumber: AbstractControl;
  alarmSender: AbstractControl;
  resourcesUsed: AbstractControl;
  numberLifeGuard: AbstractControl;
  observation:AbstractControl;

  // Sit 1 & 2
  dateEnd: AbstractControl;
  externalResourcesUsed: AbstractControl;
  isLifeGuardCall: AbstractControl;
  hourCall112: AbstractControl;
  cecoes: AbstractControl;
  delayArriveExternalResources: AbstractControl;

  // Sit 3
  evacuation: AbstractControl;
  mobilizedAgents: AbstractControl;
  
  incidentReport: IncidentReport;

  alarmantes: any[] = [];
  medios: any[] = [];
  externos: any[] = [];
  motivosEvacuacion: any[] = [];
  cuerpos: any[] = [];

  user: User;
  form1Data: any;

  constructor(
    private fb: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCrtl: LoadingController, 
    private userProvider: UserProvider,
    private alertCtrl: AlertController,
    private reportsProvider: ReportsProvider,
    private formsProvider: FormsProvider
    ) {

      this.user = this.userProvider.getUser();
      this.incidentReport = this.navParams.data.incidentReport;
      this.form1Data = this.navParams.data.form1Data;

      this.incidentForm = this.fb.group({
        'dateIni': [this.incidentReport.dateIni ? (this.addZero(this.incidentReport.dateIni.getHours())+':'+this.addZero(this.incidentReport.dateIni.getMinutes())) : null, Validators.compose([Validators.required])],
        'affectedPeopleNumber': [this.incidentReport.affectedPeopleNumber ? this.incidentReport.affectedPeopleNumber : '', Validators.compose([Validators.required, Validators.min(0), Validators.pattern("[0-9]*"), this.checkPeople.bind(this)])],
        'alarmSender': [this.incidentReport.alarmSender, Validators.compose([Validators.required])],
        'resourcesUsed': [this.formsProvider.mediosFormValue(this.incidentReport.resourcesUsed)],
        'observation': [this.incidentReport.observation],

        'dateEnd': [this.incidentReport.dateEnd ? (this.addZero(this.incidentReport.dateEnd.getHours())+':'+this.addZero(this.incidentReport.dateEnd.getMinutes())) : null, Validators.compose([this.requiredOnSit1.bind(this)])],
        'cecoes': [this.incidentReport.cecoes],
        'hourCall112': [this.incidentReport.hourCall112 ? (this.addZero(this.incidentReport.hourCall112.getHours())+':'+this.addZero(this.incidentReport.hourCall112.getMinutes())) : null, Validators.compose([this.requiredOnSit1.bind(this)])],
        'isLifeGuardCall': [this.incidentReport.isLifeGuardCall],
        'delayArriveExternalResources': [this.incidentReport.delayArriveExternalResources, Validators.compose([this.requiredOnSit1.bind(this), Validators.min(0), Validators.pattern("[0-9]*")])],

        'evacuation': [this.incidentReport.evacuation, Validators.compose([this.requiredOnSit3.bind(this)])],
        'mobilizedAgents': [this.formsProvider.cuerposFormValue(this.incidentReport.mobilizedAgents), Validators.compose([this.requiredOnSit3.bind(this)])],

        'numberLifeGuard': [this.incidentReport.numberLifeGuard, Validators.compose([this.requiredOnSit0And2.bind(this), Validators.min(0), Validators.pattern("[0-9]*")])],

        'externalResourcesUsed': [this.formsProvider.externosFormValue(this.incidentReport.externalResourcesUsed), Validators.compose([this.requiredOnSit1And2.bind(this)])],
      });

      this.dateIni = this.incidentForm.controls['dateIni'];
      this.affectedPeopleNumber = this.incidentForm.controls['affectedPeopleNumber'];
      this.alarmSender = this.incidentForm.controls['alarmSender'];
      this.resourcesUsed = this.incidentForm.controls['resourcesUsed'];
      this.numberLifeGuard = this.incidentForm.controls['numberLifeGuard'];
      this.observation = this.incidentForm.controls['observation'];
      this.dateEnd = this.incidentForm.controls['dateEnd'];
      this.externalResourcesUsed = this.incidentForm.controls['externalResourcesUsed'];
      this.isLifeGuardCall = this.incidentForm.controls['isLifeGuardCall'];
      this.hourCall112 = this.incidentForm.controls['hourCall112'];
      this.cecoes = this.incidentForm.controls['cecoes'];
      this.delayArriveExternalResources = this.incidentForm.controls['delayArriveExternalResources'];
      this.evacuation = this.incidentForm.controls['evacuation'];
      this.mobilizedAgents = this.incidentForm.controls['mobilizedAgents'];
  }

  ionViewWillEnter() {
    this.affectedPeopleNumber.setValue(parseInt(this.affectedPeopleNumber.value)+1);
    this.affectedPeopleNumber.setValue(this.affectedPeopleNumber.value-1);
    this.alarmantes = this.formsProvider.getAlarmantes();
    this.medios = this.formsProvider.getMedios();
    this.externos = this.formsProvider.getExternos();
    this.motivosEvacuacion = this.formsProvider.getMotivosEvacuacion();
    this.cuerpos = this.formsProvider.getCuerpos();           
  }

  checkPeople(control: FormGroup): { [s: string]: boolean } {
    if (this.form1Data.type == 3) {
      return null;
    }
    this.changePeopleNumber(control.value);
    for (let i = 0; i < control.value; i++) {
      if (!this.incidentReport.affectedPeople[i].valid) {
        return { checkPeople: true };
      }
    }
  }

  requiredOnSit1(control: FormGroup): { [s: string]: boolean } {
    if (control.value === null && this.form1Data.type > 0) {
      return { requiredOnSit1: true };
    }
  }

  requiredOnSit3(control: FormGroup): { [s: string]: boolean } {
    if (control.value === null && this.form1Data.type > 2) {
      return { requiredOnSit3: true };
    }
  }

  requiredOnSit0And2(control: FormGroup): { [s: string]: boolean } {
    if (control.value === null && (this.form1Data.type == 0 || this.form1Data.type == 2)) {
      return { requiredOnSit0And2: true };
    }
  }

  requiredOnSit1And2(control: FormGroup): { [s: string]: boolean } {
    if (control.value === null && (this.form1Data.type == 1 || this.form1Data.type == 2)) {
      return { requiredOnSit1And2: true };
    }
  }

  changePeopleNumber(number) {
    let length = this.incidentReport.affectedPeople.length;
    if (number > length) {
      for (let i = 0; i < number - length; i++) {
        this.incidentReport.affectedPeople.push(new AffectedPeople);
      }
    } else {
      for (let i = 0; i < length - number; i++) {
        this.incidentReport.affectedPeople.pop();
      }
    }
  }

  editProfile(index) {
    this.navCtrl.push(AffectedFormPage, {affectedPeople: this.incidentReport.affectedPeople[index], index: index, situation: this.form1Data.type});
  }

  addZero(number) {
    if (number <= 9) {
      return '0'+number;
    } else {
      return number;
    }
  }

  back() {
    this.navCtrl.popToRoot();
  }

  showError(errorMsg) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: errorMsg,
      buttons: ['OK']
    });
    alert.present();
  }

  setIncidentReport(value) {
    if(this.incidentForm.valid) {
      this.incidentReport.type = this.form1Data.type;
      this.incidentReport.lat = this.form1Data.lat;
      this.incidentReport.lng = this.form1Data.lng;
      this.incidentReport.image1 = this.form1Data.image1;
      this.incidentReport.image2 = this.form1Data.image2;
      this.incidentReport.image3 = this.form1Data.image3;
      this.incidentReport.beach = this.form1Data.beach;
      this.incidentReport.adviser = this.form1Data.adviser;
      this.incidentReport.point = this.form1Data.point;
      this.incidentReport.sync = false;

      this.incidentReport.affectedPeopleNumber = value.affectedPeopleNumber;
      this.incidentReport.alarmSender = value.alarmSender;
      this.incidentReport.resourcesUsed = [];
      if (value.resourcesUsed) {
        for (let item of value.resourcesUsed) {
          this.incidentReport.resourcesUsed.push(item.id);
        }
      }
      this.incidentReport.numberLifeGuard = value.numberLifeGuard;
      this.incidentReport.observation = value.observation;
      this.incidentReport.cecoes = value.cecoes;
      this.incidentReport.isLifeGuardCall = parseInt(value.isLifeGuardCall);
      this.incidentReport.externalResourcesUsed = [];
      if (value.externalResourcesUsed) {
        for (let item of value.externalResourcesUsed) {
          this.incidentReport.externalResourcesUsed.push(item.id);
        }
      }
      this.incidentReport.delayArriveExternalResources = value.delayArriveExternalResources;
      this.incidentReport.evacuation = value.evacuation;
      this.incidentReport.mobilizedAgents = [];
      if (value.mobilizedAgents) {
        for (let item of value.mobilizedAgents) {
          this.incidentReport.mobilizedAgents.push(item.id);
        }
      }

      let day, month, year, hours, minutes, aux;
      if (this.navParams.data.nuevo) {
        day = this.user.date.getDate();
        month = this.user.date.getMonth();
        year = this.user.date.getFullYear();
      } else {
        day = this.incidentReport.dateIni.getDate();
        month = this.incidentReport.dateIni.getMonth();
        year = this.incidentReport.dateIni.getFullYear();
      }

      this.incidentReport.dateIni = new Date();
      aux = value.dateIni.split(':');
      hours = aux[0];
      minutes = aux[1];
      this.incidentReport.dateIni.setMinutes(minutes);
      this.incidentReport.dateIni.setHours(hours);
      this.incidentReport.dateIni.setDate(day);
      this.incidentReport.dateIni.setMonth(month);
      this.incidentReport.dateIni.setFullYear(year);

      if (value.dateEnd) {
        this.incidentReport.dateEnd = new Date();
        aux = value.dateEnd.split(':');
        hours = aux[0];
        minutes = aux[1];
        this.incidentReport.dateEnd.setMinutes(minutes);
        this.incidentReport.dateEnd.setHours(hours);
        this.incidentReport.dateEnd.setDate(day);
        this.incidentReport.dateEnd.setMonth(month);
        this.incidentReport.dateEnd.setFullYear(year);
      }

      if (value.hourCall112) {
        this.incidentReport.hourCall112 = new Date();
        aux = value.hourCall112.split(':');
        hours = aux[0];
        minutes = aux[1];
        this.incidentReport.hourCall112.setMinutes(minutes);
        this.incidentReport.hourCall112.setHours(hours);
        this.incidentReport.hourCall112.setDate(day);
        this.incidentReport.hourCall112.setMonth(month);
        this.incidentReport.hourCall112.setFullYear(year);
      }

      if (this.navParams.data.nuevo) {
        this.reportsProvider.newIncidentReport(this.incidentReport);
      }

      this.reportsProvider.saveReports();

      console.log(this.incidentReport);

      this.back();
    }
  }

}
