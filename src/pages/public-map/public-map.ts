import { Component, ViewChild } from '@angular/core';
import { MapComponent } from '../../components/map/map';
import { AlertController, Loading, LoadingController, Platform, App } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { PlayaProvider } from '../../providers/playa-provider';
import { BeachDetailsPage } from '../beach-details/beach-details';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-public-map',
  templateUrl: 'public-map.html'
})
export class PublicMapPage {

  @ViewChild('map') mapComponent: MapComponent;

  loading: Loading;
  lastEvent: any;

  eventSubscribed: any;

  constructor(
    private alertCtrl: AlertController,
    private geolocation: Geolocation,
    private playaProvider: PlayaProvider,
    private loadingController: LoadingController,
    private platform: Platform, 
    private translateService: TranslateService,
    private app: App,
  ) {}

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.loading = this.loadingController.create({
        content: this.translateService.instant('GEOLOCALIZANDO')
      });
      this.loading.present();

      this.mapComponent.setDynamicIcons(true);

      let options = {enableHighAccuracy: false, timeout: 10000};

      if (!this.eventSubscribed) {
        this.eventSubscribed = this.mapComponent.$mapClick.asObservable().subscribe(evt => {
          this.lastEvent = evt;
          this.loadMapCustomPixel();
        });
      }

      this.geolocation.getCurrentPosition(options).then(
        (position) => {
          this.mapComponent.setCenter(position.coords.latitude, position.coords.longitude);
          this.mapComponent.setZoom(14);
          this.playaProvider.getPlayasData().then(
            res => {
              this.playaProvider.getCercanasData(position.coords.latitude, position.coords.longitude).then(
                res => {
                  this.mapComponent.setFlags(this.playaProvider.getPlayas());
                  this.loading.dismiss();
                  this.mapComponent.refreshMap();
                },
                error => {
                  this.mapComponent.setFlags(this.playaProvider.getPlayas());
                  this.loading.dismiss();
                  this.showError(this.translateService.instant('NET_ERROR'));
                  this.mapComponent.refreshMap();
                }
              );
            },
            error => {
              this.mapComponent.setFlags(this.playaProvider.getPlayas());
              this.loading.dismiss();
              this.showError(this.translateService.instant('NET_ERROR'));
              this.mapComponent.refreshMap();
            }
          );
        }
        ).catch((error) => {
          this.playaProvider.getPlayasData().then(
            res => {
              this.loading.dismiss();
              this.showError(this.translateService.instant('GEO_ERROR'));
              this.mapComponent.setFlags(this.playaProvider.getPlayas());
              this.mapComponent.refreshMap();
            },
            error => {
              this.loading.dismiss();
              this.showError(this.translateService.instant('NET_GEO_ERROR'));
              this.mapComponent.setFlags(this.playaProvider.getPlayas());
              this.mapComponent.refreshMap();
            }
          );
        }
      );
    });
  }

  loadMapCustomPixel(){
    if (this.lastEvent) {
      let coords = this.mapComponent.normalizeCoords(this.lastEvent.coordinate[1], this.lastEvent.coordinate[0]);
      let playaSeleccionada = this.playaProvider.getNearestToCoords(coords);
      if (playaSeleccionada) {
        console.log(playaSeleccionada);
        this.showBeachInfo(playaSeleccionada);
      }
    }
  }

  showBeachInfo(playa) {
    this.app.getRootNav().push(BeachDetailsPage, {playa: playa});
  }

  showError(errorMsg) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: this.translateService.instant('ERROR'),
      subTitle: errorMsg,
      buttons: [this.translateService.instant('OK')]
    });
    alert.present();
  }

}
