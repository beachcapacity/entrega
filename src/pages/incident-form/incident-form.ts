import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, /*Loading,*/ ModalController, MenuController, AlertController, ActionSheetController, /*Platform*/ } from 'ionic-angular';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { UserProvider, User } from '../../providers/user-provider';
import { IncidentReport } from '../../providers/reports-provider';
import { PictureSourceType, CameraOptions, Camera } from '@ionic-native/camera';
// import { DocumentViewer } from '@ionic-native/document-viewer';
import { Observable } from 'rxjs';

import { IncidentForm2Page } from '../incident-form-2/incident-form-2';
import { MapComponent } from '../../components/map/map';

@Component({
  selector: 'page-incident-form',
  templateUrl: 'incident-form.html'
})

export class IncidentFormPage {

  @ViewChild('map') mapComponent: MapComponent;

  incidentForm: FormGroup; 

  lat: AbstractControl;
  lng: AbstractControl;
  type: AbstractControl;
  image1: AbstractControl;
  image2: AbstractControl;
  image3: AbstractControl;

  incidentReport: IncidentReport;

  user: User;

  lastEvent: any;

  constructor(
    private fb: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCrtl: LoadingController, 
    public toastCrtl: ToastController, 
    public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    private userProvider: UserProvider,
    private alertCtrl: AlertController,
    public camera: Camera, 
    public asCtrl: ActionSheetController,
    // private platform: Platform,
    // private document: DocumentViewer,
    ) {

      this.user = this.userProvider.getUser();

      if (this.navParams.data.nuevo && !this.navParams.data.incidentReport) {
        this.incidentReport = new IncidentReport;
      } else if (!this.navParams.data.nuevo && this.navParams.data.incidentReport) {
        this.incidentReport = this.navParams.data.incidentReport;
      }

      this.incidentForm = this.fb.group({
        'lat': [this.incidentReport.lat, Validators.compose([Validators.required])],
        'lng': [this.incidentReport.lng, Validators.compose([Validators.required])],
        'type': [this.incidentReport.type, Validators.compose([Validators.required])],
        'image1': [this.incidentReport.image1],
        'image2': [this.incidentReport.image2],
        'image3': [this.incidentReport.image3],
      });

      this.lat = this.incidentForm.controls['lat'];
      this.lng = this.incidentForm.controls['lng'];
      this.type = this.incidentForm.controls['type'];
      this.image1 = this.incidentForm.controls['image1'];
      this.image2 = this.incidentForm.controls['image2'];
      this.image3 = this.incidentForm.controls['image3'];
  }

  ionViewWillEnter() {
    this.checkType();

    if (this.mapComponent) {
      this.mapComponent.$mapClick.asObservable().subscribe(evt => {
        this.lastEvent = evt;
        this.loadMapCustomPixel();
      });
      this.mapComponent.setZoom(18);
    }

    if (!this.incidentReport.lat || !this.incidentReport.lng) {
      this.lat.setValue(this.user.playa.latitude);
      this.lng.setValue(this.user.playa.longitude);
      if (this.mapComponent) {
        this.mapComponent.setCenter(parseFloat(this.user.playa.latitude), parseFloat(this.user.playa.longitude));
      }
    } else {
      if (this.mapComponent) {
        this.mapComponent.setCenter(this.incidentReport.lat, this.incidentReport.lng);
        this.mapComponent.setMarker(this.mapComponent.transformProj(this.incidentReport.lat, this.incidentReport.lng));
      }
    }
  }

  loadMapCustomPixel(){
    if (this.lastEvent) {
      this.mapComponent.setMarker(this.lastEvent.coordinate);
      let coords = this.mapComponent.normalizeCoords(this.lastEvent.coordinate[1], this.lastEvent.coordinate[0]);

      this.lat.setValue(coords[1]);
      this.lng.setValue(coords[0]);
    }
  }

  back() {
    this.navCtrl.pop();
  }

  showAc(){
    this.asCtrl.create({
    buttons: [
    {
      text: 'Desde la cámara',
      handler: () => {
        this.takePhoto(PictureSourceType.CAMERA).subscribe(photo => {
          if (photo){
            this.addPhoto(photo);
          }
        });
      }
    },{
      text: 'Desde la galería',
      handler: () => {
        this.takePhoto(PictureSourceType.PHOTOLIBRARY).subscribe(photo => {
          if (photo){
            this.addPhoto(photo);
          }
        });
      }
    },{
      text: 'Cancelar',
      role: 'cancel'
    }
    ]
  }).present();
  }

  takePhoto(source: PictureSourceType): Observable<any>{
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      cameraDirection: this.camera.Direction.BACK,
      allowEdit: false,
      correctOrientation: true,
      saveToPhotoAlbum: false,
      targetHeight: 800,
      targetWidth: 600,
      sourceType: source
    }
    return Observable.fromPromise(this.camera.getPicture(options))
    .map(result => {
      return result;
    })
    .catch(() => Observable.of<any>(null));
  }

  addPhoto(photo) {
    if (!this.image1.value) {
      this.image1.setValue(photo);
    } else if (!this.image2.value) {
      this.image2.setValue(photo);
    } else if (!this.image3.value) {
      this.image3.setValue(photo);
    }
  }

  removePhoto(number) {
    if (number == 3) {
      this.image3.setValue(null);
    } else if (number == 2) {
      this.image2.setValue(this.image3.value);
      this.image3.setValue(null);
    } else if (number == 1) {
      this.image1.setValue(this.image2.value);
      this.image2.setValue(this.image3.value);
      this.image3.setValue(null);
    }
  }

  showError(errorMsg) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: errorMsg,
      buttons: ['OK']
    });
    alert.present();
  }

  changeType(type) {
    if (this.type.value !== null) {
      document.getElementById('type-'+this.type.value).classList.remove('type-button-active');
    }
    document.getElementById('type-'+type).classList.add('type-button-active');
    this.type.setValue(type);
  }

  setType(value) {
    if(this.incidentForm.valid) {
      let form1Data = {
        type: value.type,
        lat: parseFloat(value.lat),
        lng: parseFloat(value.lng),
        image1: value.image1,
        image2: value.image2,
        image3: value.image3,
        beach: this.incidentReport.beach ? this.incidentReport.beach : this.user.playa.id,
        adviser: this.user.id,
        point: this.incidentReport.point ? this.incidentReport.point : "POINT("+value.lat+" "+value.lng+")",
      }

      this.navCtrl.push(IncidentForm2Page, {nuevo: this.navParams.data.nuevo, incidentReport: this.incidentReport, form1Data: form1Data})
    }
  }

  checkType() {
    if (this.type.value !== null) {
      document.getElementById('type-'+this.type.value).classList.add('type-button-active');
    }
  }

  openDecreto() {
    // if (this.platform.is('android')) {
    //   this.document.viewDocument('file:///android_asset/www/assets/decreto.pdf', 'application/pdf', {title: 'Decreto'})
    // } else {
    //   this.document.viewDocument('assets/pdf/decreto.pdf', 'application/pdf', {title: 'Decreto'})
    // }
  }

}
