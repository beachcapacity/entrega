import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Loading, App } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LocationPage } from '../location/location';
import { LocalNotifications } from  '@ionic-native/local-notifications';

import { IncidentReportsPage } from '../incident-reports/incident-reports';
import { PeopleReportsPage } from '../people-reports/people-reports';
import { FlagReportsPage } from '../flag-reports/flag-reports';

import {ReportsProvider } from '../../providers/reports-provider';
import { UserProvider } from '../../providers/user-provider';

import { UpdatePage } from '../update/update';
import { FormsProvider } from '../../providers/forms-provider';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';

interface AppInfo {
  appName: any;
  packageName: any;
  versionCode: any;
  versionNumber: any;
  device: Device;
  
}

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  flagReports = FlagReportsPage;
  issueReports = IncidentReportsPage;
  peopleReports = PeopleReportsPage;

  loading: Loading;

  appInfo: AppInfo;
  minVersion: any;

  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private localNotifications: LocalNotifications,
    private reportsProvider: ReportsProvider,
    private loadingController: LoadingController,
    private userProvider: UserProvider,
    private formsProvider: FormsProvider,
    private appVersion: AppVersion, 
    public device: Device, 
  ) {}


  ionViewDidLoad() {
    this.setAviso();
  }

  ionViewDidEnter() {
    this.minVersion = this.formsProvider.getGeneralConfig()['min_app_version'];
    console.log(this.minVersion);
    
    if (this.minVersion) {

      Promise.all([
        this.appVersion.getAppName(),
        this.appVersion.getPackageName(),
        this.appVersion.getVersionCode(),
        this.appVersion.getVersionNumber(),
      ]).then(data => {
        console.log(data);
        this.appInfo = {
          appName: data[0],
          packageName: data[1],
          versionCode: data[2],
          versionNumber: data[3],
          device: this.device,
        };
  
        console.log('getAppInfo (appName)', this.appInfo.appName);
        console.log('getAppInfo (packageName)', this.appInfo.packageName);
        console.log('getAppInfo (versionCode)', this.appInfo.versionCode);
        console.log('getAppInfo (versionNumber)', this.appInfo.versionNumber);
        console.log('getAppInfo (device)', this.appInfo.device);
        console.log('getServerInfo (minVersion)', this.minVersion);

        if (this.appInfo.versionCode < this.minVersion) {
          this.navCtrl.setRoot(UpdatePage);
        }
  
      });
    }
  }

  resumeEvent(){
    this.setAviso();
  }
  
  syncReports() {
    this.loading = this.loadingController.create({
      content: 'Sincronizando...'
    });
    this.loading.present();

    this.reportsProvider.sync().then(
      res => {
        let alert = this.alertCtrl.create({
          title: 'OK!',
          subTitle: 'Informes Sincronizados con el servidor.',
          buttons: ['OK']
        });
        this.loading.dismiss();
        alert.present(); 
      },
      error => {
        this.showError('Algunos informes no se han podido sincronizar, inténtelo de nuevo más tarde.');
      }
    );
  }

  logout() {
    let alert = this.alertCtrl.create({
      title: 'Salir',
      message: '¿Está seguro que desea salir?',
      buttons: [
          {
              text: 'Cancelar',
              handler: () => {
                  alert.dismiss();
                  return false;
              }
          },
          {
            text: 'Cambiar playa',
            handler: () => {
                alert.dismiss().then(() => {
                  this.navCtrl.setRoot(LocationPage);
                });
            }
          },
          {
              text: 'Cerrar sesión',
              handler: () => {
                  this.userProvider.logout();
                  alert.dismiss().then(() => {
                    this.navCtrl.setRoot(HomePage);
                  });
              }
          }
      ]
    });
    alert.present();
  }

  setAviso() {
    let date = new Date();
    if (date.getHours() < 12) {
      date.setHours(12);
      date.setMinutes(0);
      this.localNotifications.schedule({
        id: 1,
        title: 'Recordatorio informe de ocupación 12:00',
        trigger: {at: date}
      });
    }
    if (date.getHours() < 14) {
      date.setHours(14);
      date.setMinutes(0);
      this.localNotifications.schedule({
        id: 2,
        title: 'Recordatorio informe de ocupación 14:00',
        trigger: {at: date}
      });
    }
  }

  showError(errorMsg) {
    this.loading.dismiss();
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: errorMsg,
      buttons: ['OK']
    });
    alert.present();
  }

}