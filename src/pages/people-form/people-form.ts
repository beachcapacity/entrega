import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, /*Loading,*/ ModalController, MenuController, AlertController } from 'ionic-angular';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { UserProvider, User } from '../../providers/user-provider';
import { ReportsProvider, PeopleReport } from '../../providers/reports-provider';
import { FormsProvider } from '../../providers/forms-provider';

@Component({
  selector: 'page-people-form',
  templateUrl: 'people-form.html'
})

export class PeopleFormPage {

  peopleForm: FormGroup; 

  lvl: AbstractControl;
  temperature: AbstractControl;
  windOrientation: AbstractControl;
  windSpeed: AbstractControl;

  peopleReport: PeopleReport;

  user: User;
  ocupacion_media: number;
  ocupacion_alta: number;

  constructor(
    private fb: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCrtl: LoadingController, 
    public toastCrtl: ToastController, 
    public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    private userProvider: UserProvider,
    private formsProvider: FormsProvider,
    private alertCtrl: AlertController,
    private reportsProvider: ReportsProvider,
    ) {

      this.user = this.userProvider.getUser();
      this.ocupacion_media = this.formsProvider.getGeneralConfig()['ocupacion_media'];
      this.ocupacion_alta = this.formsProvider.getGeneralConfig()['ocupacion_alta'];

      if (this.navParams.data.nuevo && !this.navParams.data.peopleReport) {
        this.peopleReport = new PeopleReport;
      } else if (!this.navParams.data.nuevo && this.navParams.data.peopleReport) {
        this.peopleReport = this.navParams.data.peopleReport;
      } else {
        this.peopleReport = new PeopleReport;
        this.peopleReport.lvl = this.navParams.data.peopleReport.lvl;
        this.peopleReport.temperature = this.navParams.data.peopleReport.temperature;
        this.peopleReport.windOrientation = this.navParams.data.peopleReport.windOrientation;
        this.peopleReport.windSpeed= this.navParams.data.peopleReport.windSpeed;
      }

      this.peopleForm = this.fb.group({
        'lvl': [this.peopleReport.lvl, Validators.compose([Validators.required])],
        'temperature': [this.peopleReport.temperature ? this.peopleReport.temperature : 25],
        'windOrientation': [this.peopleReport.windOrientation],
        'windSpeed': [this.peopleReport.windSpeed ? this.peopleReport.windSpeed : 0],
      });

      this.lvl = this.peopleForm.controls['lvl'];
      this.temperature = this.peopleForm.controls['temperature'];
      this.windOrientation = this.peopleForm.controls['windOrientation'];
      this.windSpeed = this.peopleForm.controls['windSpeed'];
  }

  ionViewWillEnter() {
    this.checkLvl();
  }

  back() {
    this.navCtrl.pop();
  }

  showError(errorMsg) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: errorMsg,
      buttons: ['OK']
    });
    alert.present();
  }

  changePeople(people) {
    if (this.lvl.value !== null) {
      document.getElementById('people-'+this.lvl.value).classList.remove('people-button-active');
    }
    
     
     if(this.lvl.value === 0) {
      const b  = document.getElementById('baja');
    b.style.color = 'black' ;
    }
  
    if(this.lvl.value === 1) {
      const b  = document.getElementById('baja');
    b.style.color = 'white' ;
    }
    if(this.lvl.value === 2) {
      const b  = document.getElementById('baja');
    b.style.color = 'white' ;
    }
    if(this.lvl.value === 3) {
      const b  = document.getElementById('baja');
    b.style.color = 'white' ;
    }
    
    /* Botones cambio de colores intento  */
   /* if(people.length !== 0) {
      const b  = document.getElementById('baja');
    b.style.color = 'white' ;
    }
    if(this.lvl.value !== 0) {
      const b  = document.getElementById('baja');
    b.style.color = 'white' ;
    }
    if(this.lvl.value !== 1) {
      const m  = document.getElementById('media');
    m.style.color = 'white' ;
    }
    if(this.lvl.value !== 2) {
      const a  = document.getElementById('alta');
    a.style.color = 'white' ;
    }
    if(this.lvl.value !== 3) {
      const c  = document.getElementById('cerrada');
    c.style.color = 'white' ;
    }
    else{
      const b  = document.getElementById('baja');
      b.style.color = 'black' ;
      const m  = document.getElementById('media');
    m.style.color = 'black' ;
    const a  = document.getElementById('alta');
    a.style.color = 'black' ;
    const c  = document.getElementById('cerrada');
    c.style.color = 'black' ;
    }*/
    document.getElementById('people-'+people).classList.add('people-button-active');
    this.lvl.setValue(people);
  }

  setPeopleReport(value) {
    if(this.peopleForm.valid) {
      this.peopleReport.lvl = value.lvl;
      this.peopleReport.temperature = value.temperature;
      this.peopleReport.windSpeed = value.windSpeed;
      this.peopleReport.windOrientation = value.windOrientation;

      this.peopleReport.beach = this.peopleReport.beach ? this.peopleReport.beach : this.user.playa.id;
      this.peopleReport.adviser = this.user.id;
      this.peopleReport.point = this.peopleReport.point ? this.peopleReport.point : "POINT("+this.user.playa.latitude+" "+this.user.playa.longitude+")";
      this.peopleReport.date = this.peopleReport.date ? this.peopleReport.date : new Date();
      this.peopleReport.sync = false;

      if (this.navParams.data.nuevo) {
        this.reportsProvider.newPeopleReport(this.peopleReport);
      }

      this.reportsProvider.saveReports();

      console.log(this.peopleReport);

      this.back();
    }
  }

  checkLvl() {
    if (this.lvl.value !== null) {
      document.getElementById('people-'+this.lvl.value).classList.add('people-button-active');
    }
  }

}
