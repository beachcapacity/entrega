import { Component } from '@angular/core';
import { NavController, App } from 'ionic-angular';

import { PlayaProvider, Playa } from '../../providers/playa-provider';
import { BeachDetailsPage } from '../beach-details/beach-details';

@Component({
  selector: 'page-public-list',
  templateUrl: 'public-list.html'
})
export class PublicListPage {

  playas: Playa[] = [];
  playasFull: Playa[] = [];

  islas: any[] = []
  islaSel: string;

  municipios: any[] = [];
  municipioSel: string;

  constructor(
    public navCtrl: NavController,
    private playaProvider: PlayaProvider,
    private app: App,
  ) {

  }

  ionViewWillEnter() {
    this.playasFull = this.playaProvider.getPlayasCercanas();
    this.playas = [...this.playasFull];
    console.log(this.playas);
    this.islas = this.playaProvider.getIslas();
    console.log(this.islas);
  }

  searchBeach(ev: any) {

    // set val to the value of the searchbar
    let val = ev.target.value;
    val = val.trim();

    // if the value is an empty string don't filter the items
    if (val) {
      
      console.log("Search. Value", val);

      this.playas = [...this.playasFull];

      if (this.playas){
        this.playas = this.playas.filter((item) => {
          return (item.island && this.removeAccent(item.island).indexOf(val.toLowerCase()) > -1);
        });
        if (!this.playas.length) {
          this.playas = [...this.playasFull];
          this.playas = this.playas.filter((item) => {
            return (item.municipality && this.removeAccent(item.municipality).indexOf(val.toLowerCase()) > -1);
          });
          if (!this.playas.length) {
            this.playas = [...this.playasFull];
            this.playas = this.playas.filter((item) => {
              return (item.name && this.removeAccent(item.name).indexOf(val.toLowerCase()) > -1);
            });
          }
        }
      }
    }
    else {
      this.onCancelSearchContacts(null);
    }
  }

  changeIsland($event) {
    this.municipioSel = '';
    this.municipios = [];
    if ($event) {
      this.playas = [...this.playasFull];
      if (this.playas){
        this.playas = this.playas.filter((item) => {
          return (item.island && item.island.indexOf($event) > -1);
        });
        this.loadMunicipalities($event);
      }
    } else {
      this.onCancelSearchContacts(null);
    }
  }

  changeMunicipality($event) {
    if ($event) {
      this.playas = [...this.playasFull];
      if (this.playas){
        this.playas = this.playas.filter((item) => {
          return (item.island && item.island.indexOf(this.islaSel) > -1);
        });
        this.playas = this.playas.filter((item) => {
          return (item.municipality && item.municipality.indexOf($event) > -1);
        });
      }
    } else {
      this.changeIsland(this.islaSel);
    }
  }

  loadMunicipalities(island) {
    this.municipios = [];
    this.playaProvider.getMunicipios().forEach(element => {
      if (element.island == island) {
        this.municipios.push(element.name);
      }
    });
  }

  onCancelSearchContacts(ev: any) {
    // Reset items back to all of the items
    this.playas = [...this.playasFull];
  }

  removeAccent(str): string {
    var map = {
        'a' : 'á|à|ã|â|À|Á|Ã|Â',
        'e' : 'é|è|ê|É|È|Ê',
        'i' : 'í|ì|î|Í|Ì|Î',
        'o' : 'ó|ò|ô|õ|Ó|Ò|Ô|Õ',
        'u' : 'ú|ù|û|ü|Ú|Ù|Û|Ü',
        'c' : 'ç|Ç',
        'n' : 'ñ|Ñ'
    };
    
    str = str.toLowerCase();
    
    for (var pattern in map) {
        str = str.replace(new RegExp(map[pattern], 'g'), pattern);
    }

    return str;
  }

  addZero(number) {
    if (number <= 9) {
      return '0'+number;
    } else {
      return number;
    }
  }

  // beaufortToKmh(beaufort) {
  //   var kmhLimits = ['0~1','2~6','7~11','12~19','20~30','31~39','40~50','51~61','62~74','75~87','88~102','103~117','>117'];
  //   if (beaufort === null)
  //     return "Desconocida";
  //   return kmhLimits[beaufort]+' km/h';
  // }

  showBeachInfo(playa) {
    this.app.getRootNav().push(BeachDetailsPage, {playa: playa});
  }

}
