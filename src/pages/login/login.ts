import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading, ModalController, AlertController, MenuController } from 'ionic-angular';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { UserProvider } from '../../providers/user-provider';

import { LocationPage } from '../location/location';
import { TranslateService } from '@ngx-translate/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

  loading: Loading;

  r_username: string;
  r_password: string;
  r_remember: string;

  authForm: FormGroup; 
  username: AbstractControl;
  password: AbstractControl;
  remember: AbstractControl;

  constructor(
    private fb: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCrtl: LoadingController, 
    public toastCrtl: ToastController, 
    public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    private userProvider: UserProvider,
    private alertCtrl: AlertController,
    private translateService: TranslateService,
    private iab: InAppBrowser
    ) {

    this.r_username = localStorage.getItem('username');
    this.r_password = localStorage.getItem('password');
    this.r_remember = localStorage.getItem('remember');

    this.authForm = this.fb.group({
      'username': [this.r_username, Validators.compose([Validators.required, Validators.minLength(1)])],
      'password': [this.r_password, Validators.compose([Validators.required, Validators.minLength(1)])],
      'remember': [this.r_remember]
    });

    this.username = this.authForm.controls['username'];
    this.password = this.authForm.controls['password'];
    this.remember = this.authForm.controls['remember'];
  }

  tryLogin(value) {
    this.manageRemember();

    if(this.authForm.valid) {
      this.loading = this.loadingCrtl.create({
        content: this.translateService.instant('ENTRANDO')
      });
      this.loading.present();

      let request = {
        username: value.username.trim(),
        password: value.password.trim(),
        grant_type: 'password',
        client_id: '1_5ivyoetdzbgocgsosokg40ckckoggwgo4s08wwcwcwwcsccogo',
        client_secret: '18emxrnzh8v4gskssksk4sw4880sooo04wkco4wkccgwgskkwo'
      }

      this.userProvider.login(request).subscribe(
        loggedin => {
          console.log(loggedin);
          if (loggedin) {
            this.userProvider.getUserData().subscribe(
              ok => {
                this.manageRemember();
                this.loading.dismiss();
                this.navCtrl.setRoot(LocationPage);
              },
              error => {
                this.loading.dismiss();
                this.showError(this.translateService.instant('NET_ERROR'));
              }
            );
          } else {
            this.loading.dismiss();
            this.showError(this.translateService.instant('CREDENCIALES_ERROR'));
          }
        },
        error => {
          this.loading.dismiss();
          this.showError(this.translateService.instant('NET_CREDENCIALES_ERROR'));
        }
      );
    }
  }

  manageRemember() {
    if (this.remember.value) {
      localStorage.setItem('username', this.username.value);
      localStorage.setItem('password', this.password.value);
      localStorage.setItem('remember', this.remember.value);
      this.userProvider.saveUser()
    } else {
      localStorage.removeItem('username');
      localStorage.removeItem('password');
      localStorage.removeItem('remember');
      localStorage.removeItem('user');
    }
  }

  offlineMode() {
    if (this.userProvider.getUser().access_token) {
      this.navCtrl.setRoot(LocationPage);
    } else {
      this.showError(this.translateService.instant('OFFLINE_ERROR'));
    }
  }

  back() {
    this.navCtrl.pop();
  }

  showError(errorMsg) {
    let alert = this.alertCtrl.create({
      title: this.translateService.instant('ERROR'),
      subTitle: errorMsg,
      buttons: [this.translateService.instant('OK')]
    });
    alert.present();
  }

  openForgot() {
    this.iab.create('http://gestioninfoplayascanarias.d998.dinaserver.com/admin/resetting/request', '_system');
  }
}
