import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, Loading, ModalController, MenuController } from 'ionic-angular';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { UserProvider, User } from '../../providers/user-provider';
import { ReportsProvider, FlagReport } from '../../providers/reports-provider';
import { FormsProvider } from '../../providers/forms-provider';
//import {PDFDocument} from 'pdf-lib';



@Component({
  selector: 'page-flag-form',
  templateUrl: 'flag-form.html'
})



export class FlagFormPage {

  loading: Loading;

  flagForm: FormGroup; 

  flag: AbstractControl;
  date: AbstractControl;
  dateRevision: AbstractControl;
  dateArriado: AbstractControl;
  flagExtra: AbstractControl;
  observation: AbstractControl;
  reason: AbstractControl;
  dangerReason: AbstractControl;
  temperature: AbstractControl;
  windOrientation: AbstractControl;
  windSpeed: AbstractControl;

  flagReport: FlagReport;

  now: Date = new Date;

  motivos: any[] = [];
  peligros: any[] = [];

  user: User;

  timeoutHandler: any;

  constructor(
    private fb: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCrtl: LoadingController, 
    public toastCrtl: ToastController, 
    public modalCtrl: ModalController,
    public menuCtrl: MenuController,
    private userProvider: UserProvider,
    private reportsProvider: ReportsProvider,
    private formsProvider: FormsProvider
    ) {

      this.user = this.userProvider.getUser();
      
      if (this.navParams.data.nuevo && !this.navParams.data.flagReport) {
        this.flagReport = new FlagReport;
      } else if (!this.navParams.data.nuevo && this.navParams.data.flagReport) {
        this.flagReport = this.navParams.data.flagReport;
      } else {
        this.flagReport = new FlagReport;
        this.flagReport.flag = this.navParams.data.flagReport.flag;
        this.flagReport.date = this.navParams.data.flagReport.date;
        this.flagReport.dateRevision = this.navParams.data.flagReport.dateRevision;
        this.flagReport.dateArriado = this.navParams.data.flagReport.dateArriado;
        this.flagReport.flagExtra = this.navParams.data.flagReport.flagExtra;
        this.flagReport.observation = this.navParams.data.flagReport.observation;
        this.flagReport.reason = this.navParams.data.flagReport.reason;
        this.flagReport.dangerReason = this.navParams.data.flagReport.dangerReason;
        this.flagReport.temperature = this.navParams.data.flagReport.temperature;
        this.flagReport.windOrientation = this.navParams.data.flagReport.windOrientation;
        this.flagReport.windSpeed= this.navParams.data.flagReport.windSpeed;
      }

      let auxDate = new Date();

      this.flagForm = this.fb.group({
        'flag': [this.flagReport.flag, Validators.compose([Validators.required])],
        'date': [this.flagReport.date ? (this.addZero(this.flagReport.date.getHours())+':'+this.addZero(this.flagReport.date.getMinutes())) : (this.addZero(auxDate.getHours())+':'+this.addZero(auxDate.getMinutes())), Validators.compose([Validators.required])],
        'dateRevision': [this.flagReport.dateRevision ? (this.addZero(this.flagReport.dateRevision.getHours())+':'+this.addZero(this.flagReport.dateRevision.getMinutes())) : null],
        'dateArriado': [this.flagReport.dateArriado ? (this.addZero(this.flagReport.dateArriado.getHours())+':'+this.addZero(this.flagReport.dateArriado.getMinutes())) : null],
        'flagExtra': [this.flagReport.flagExtra],
        'observation': [this.flagReport.observation],
        'reason': [this.flagReport.reason, Validators.compose([Validators.required])],
        'dangerReason': [this.flagReport.dangerReason],
        'temperature': [this.flagReport.temperature ? this.flagReport.temperature : 25, Validators.compose([Validators.required])],
        'windOrientation': [this.flagReport.windOrientation, Validators.compose([Validators.required])],
        'windSpeed': [this.flagReport.windSpeed ? this.flagReport.windSpeed : 0, Validators.compose([Validators.required])],
      });

      this.flag = this.flagForm.controls['flag'];
      this.date = this.flagForm.controls['date'];
      this.dateRevision = this.flagForm.controls['dateRevision'];
      this.dateArriado = this.flagForm.controls['dateArriado'];
      this.flagExtra = this.flagForm.controls['flagExtra'];
      this.observation = this.flagForm.controls['observation'];
      this.reason = this.flagForm.controls['reason'];
      this.dangerReason = this.flagForm.controls['dangerReason'];
      this.temperature = this.flagForm.controls['temperature'];
      this.windOrientation = this.flagForm.controls['windOrientation'];
      this.windSpeed = this.flagForm.controls['windSpeed'];

      this.dangerReason.disable();
  }

  ionViewWillEnter() {
    this.checkFlags();
    this.motivos = this.formsProvider.getMotivos();
    this.peligros = this.formsProvider.getPeligros();
  }

  back() {
    this.navCtrl.pop();
  }

  changeFlag(flag) {
    if (this.flag.value !== null) {
      document.getElementById('flag-'+this.flag.value).classList.remove('flag-button-active');
    }
    document.getElementById('flag-'+flag).classList.add('flag-button-active');
    this.flag.setValue(flag);
  }

  changeFlagExtra(flag) {
    if (this.flagExtra.value !== null) {
      document.getElementById('flag-extra-'+this.flagExtra.value).classList.remove('flag-button-active');
    }
    if (this.flagExtra.value != flag) {
      document.getElementById('flag-extra-'+flag).classList.add('flag-button-active');
      this.flagExtra.setValue(flag);
    } else {
      this.flagExtra.setValue(null);
    }
    if (this.flagExtra.value == 3) {
      this.dangerReason.enable();
    } else {
      this.dangerReason.disable();
      this.dangerReason.setValue(null);
    }
  }

  checkFlags() {
    if (this.flag.value !== null) {
      document.getElementById('flag-'+this.flag.value).classList.add('flag-button-active');
    }
    if (this.flagExtra.value !== null) {
      document.getElementById('flag-extra-'+this.flagExtra.value).classList.add('flag-button-active');
      if (this.flagExtra.value == 3) {
        this.dangerReason.enable();
      }
    }
  }

  addTemperaturePressed() {
    this.timeoutHandler = setInterval(() => {
      this.addTemperature();
    }, 100);
  }

  addTemperature() {
    this.temperature.setValue(this.temperature.value + 1);
  }

  subtractTemperaturePressed() {
    this.timeoutHandler = setInterval(() => {
      this.subtractTemperature();
    }, 100);
  }

  subtractTemperature() {
      this.temperature.setValue(this.temperature.value - 1);
  }

  addWindSpeedPressed() {
    this.timeoutHandler = setInterval(() => {
      this.addWindSpeed();
    }, 100);
  }

  addWindSpeed() {
    this.windSpeed.setValue(this.windSpeed.value + 1);
  }

  subtractWindSpeedPressed() {
    this.timeoutHandler = setInterval(() => {
      this.subtractWindSpeed();
    }, 100);
  }

  subtractWindSpeed() {
    if (this.windSpeed.value > 0)
      this.windSpeed.setValue(this.windSpeed.value - 1);
  }

  pressEnd() {
    if (this.timeoutHandler) {
      clearTimeout(this.timeoutHandler);
      this.timeoutHandler = null;
    }
  }

  addZero(number) {
    if (number <= 9) {
      return '0'+number;
    } else {
      return number;
    }
  }

  setFlagReport(value) {
    if(this.flagForm.valid) {
      this.flagReport.flag = value.flag;
      this.flagReport.reason = value.reason;
      this.flagReport.flagExtra = value.flagExtra;
      this.flagReport.dangerReason = value.dangerReason;
      this.flagReport.observation = value.observation;
      this.flagReport.temperature = value.temperature;
      this.flagReport.windSpeed = value.windSpeed;
      this.flagReport.windOrientation = value.windOrientation;

      this.flagReport.beach = this.flagReport.beach ? this.flagReport.beach : this.user.playa.id;
      this.flagReport.adviser = this.user.id;
      this.flagReport.point = this.flagReport.point ? this.flagReport.point : "POINT("+this.user.playa.latitude+" "+this.user.playa.longitude+")";
      this.flagReport.sync = false;

      let day, month, year, hours, minutes, aux;
      if (this.navParams.data.nuevo) {
        day = this.user.date.getDate();
        month = this.user.date.getMonth();
        year = this.user.date.getFullYear();
      } else {
        day = this.flagReport.date.getDate();
        month = this.flagReport.date.getMonth();
        year = this.flagReport.date.getFullYear();
      }

      this.flagReport.date = new Date();
      aux = value.date.split(':');
      hours = aux[0];
      minutes = aux[1];
      this.flagReport.date.setMinutes(minutes);
      this.flagReport.date.setHours(hours);
      this.flagReport.date.setDate(day);
      this.flagReport.date.setMonth(month);
      this.flagReport.date.setFullYear(year);

      if (value.dateRevision) {
        this.flagReport.dateRevision = new Date();
        aux = value.dateRevision.split(':');
        hours = aux[0];
        minutes = aux[1];
        this.flagReport.dateRevision.setMinutes(minutes);
        this.flagReport.dateRevision.setHours(hours);
        this.flagReport.dateRevision.setDate(day);
        this.flagReport.dateRevision.setMonth(month);
        this.flagReport.dateRevision.setFullYear(year);
      }

      if (value.dateArriado) {
        this.flagReport.dateArriado = new Date();
        aux = value.dateArriado.split(':');
        hours = aux[0];
        minutes = aux[1];
        this.flagReport.dateArriado.setMinutes(minutes);
        this.flagReport.dateArriado.setHours(hours);
        this.flagReport.dateArriado.setDate(day);
        this.flagReport.dateArriado.setMonth(month);
        this.flagReport.dateArriado.setFullYear(year);
      }

      if (this.navParams.data.nuevo) {
        this.reportsProvider.newFlagReport(this.flagReport);
      }

      console.log(this.flagReport);

      this.reportsProvider.saveReports();

      this.back();
    }
  }
}
