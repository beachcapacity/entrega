import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { MapComponent } from '../../components/map/map';
import { Playa } from '../../providers/playa-provider';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { CoastalInfoDisplayPage } from '../coastal-info-display/coastal-info-display';
import { TranslateService } from '@ngx-translate/core';
import { UVProvider } from '../../providers/uv-provider';

@Component({
  selector: 'page-beach-details',
  templateUrl: 'beach-details.html'
})

export class BeachDetailsPage {

  playa: Playa;
  uv: string;

  @ViewChild('map') mapComponent: MapComponent;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,  
    private iab: InAppBrowser,
    private translateService: TranslateService,
    private uvProvider: UVProvider
    ) {
      this.playa = this.navParams.get('playa');
      //console.log(this.playa);
      console.log("playas")
    }

  ionViewWillEnter() {
    if (this.mapComponent) {
      this.mapComponent.setCenter(parseFloat(this.playa.latitude), parseFloat(this.playa.longitude));
      this.mapComponent.setZoom(18);
      this.mapComponent.setMarker(this.mapComponent.transformProj(parseFloat(this.playa.latitude), parseFloat(this.playa.longitude)));
    }

    this.uv = this.translateService.instant('DESCONOCIDO');
    this.callUVProvider();
  }

  back() {
    this.navCtrl.pop();
  }

  openMareas() {
    let island_code = '6527';

    if (this.playa.island == "El Hierro") {
      island_code = '7131';
    } else if (this.playa.island == "Fuerteventura") {
      island_code = '6527';
    } else if (this.playa.island == "Gran Canaria") {
      island_code = '6527';
    } else if (this.playa.island == "La Gomera") {
      island_code = '7131';
    } else if (this.playa.island == "Lanzarote") {
      island_code = '6527';
    } else if (this.playa.island == "La Palma") {
      island_code = '7131';
    }  else if (this.playa.island == "Tenerife") {
      island_code = '7131';
    }

    this.navCtrl.push(CoastalInfoDisplayPage, {
      isla: island_code
    });
  }

  openAemet() {
    this.iab.create(this.playa.aemet, '_system');
  }

  openSalud() {
    this.iab.create(
      "http://www3.gobiernodecanarias.org/sanidad/scs/MapaPlayasWeb/playa.xhtml?codigo="+this.playa.id_sanidad, 
      '_system'
    );
  }

  callUVProvider() {
    this.uvProvider.call(this.playa.latitude, this.playa.longitude).then(
      res => {
        console.log(res);
        if (res.status == 200) {
          let data = JSON.parse(res.data);
          this.uv = parseInt(data.value)+'';
        }
      }
    );
  }

  secondsToTime(miliSeconds) {
    let hours = miliSeconds / 3600000;
    let minutes = miliSeconds % 3600000;
    minutes = minutes / 60000;
    if (minutes) {
      hours = Math.trunc(hours);
    }

    return (this.addZero(hours)+':'+this.addZero(minutes));
  }

  addZero(number) {
    if (number <= 9) {
      return '0'+number;
    } else {
      return number;
    }
  }

}
