import { NgModule } from '@angular/core';
import { MapComponent } from './map/map';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';

@NgModule({
  declarations: [
    MapComponent
  ],
  imports: [CommonModule, IonicModule],
  exports: [
    MapComponent
  ],
})
export class ComponentsModule {}
