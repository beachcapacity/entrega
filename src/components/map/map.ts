import { Component, ElementRef} from '@angular/core';
import {
  control,
  Feature,
  geom,
  interaction,
  layer,
  Map,
  MapBrowserEvent,
  proj,
  source,
  style,
  View,
} from 'openlayers';

import proj4 from 'proj4';

import { Subject } from 'rxjs/Subject';

import {LayersProvider} from "../../providers/layers";

@Component({
  selector: 'ip-map',
  templateUrl: 'map.html'
})
export class MapComponent {

  public instance: Map;

  public $mapClick: Subject<MapBrowserEvent>;

  controls: control.Control[] = [];
  interactions = interaction.defaults().extend([new interaction.DragRotateAndZoom()]);
  view: View;
  renderer = 'canvas';
  loadTilesWhileAnimating: true;
  loadTilesWhileInteracting: true;
  center: [number, number] = [421559, 3130695];
  marker: layer.Vector;
  flags: layer.Vector[] = [];
  playas: any[];
  wktReader: any;
  dynIcons: boolean = false;

  constructor(private host: ElementRef, private layersProvider: LayersProvider) { }

  ngOnInit() {
    proj.setProj4(proj4);
    if (!proj.get('EPSG:32628')) proj4.defs('EPSG:32628',"+proj=utm +zone=28 +ellps=WGS84 +datum=WGS84 +units=m +no_defs");
    this.view = new View({
      projection: proj.get('EPSG:32628'),
      center: this.center,
      zoom: 6,
      minZoom: 6
    });
    this.instance = new Map(this);
    this.instance.setTarget(this.host.nativeElement.firstElementChild);
    this.instance.addLayer(this.layersProvider.baseLayer.layer);

    this.$mapClick = new Subject();
    this.instance.on('click', (event: MapBrowserEvent) => this.$mapClick.next(event));
    var este = this;
    this.instance.on('moveend', function(e) {
      if (este.dynIcons) {
        if (este.instance.getView().getZoom() > 14.5) {
          este.setFlags(este.playas, 2);
        } else {
          este.setFlags(este.playas, 1);
        }
      }
    });
  }

  transformProj(lat: number, lng: number){
    return proj.transform([lng,lat], 'EPSG:4326', 'EPSG:32628')
  }

  normalizeCoords(lat: number, lng: number){
    return proj.transform([lng,lat], 'EPSG:32628', 'EPSG:4326')
  }

  setMarker(position: [number, number]){
    if (!this.instance) return;
    if (this.marker) this.instance.removeLayer(this.marker);
    //marker
    let icon = new Feature(new geom.Point(position));
    let iconStyle = new style.Style({
      image: new style.Icon({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: 'assets/imgs/point.png',
      })
    });
    icon.setStyle(iconStyle);
    let iconSource = new source.Vector({
      features: [icon]
    });
    this.marker = new layer.Vector({
      source: iconSource
    });
    this.instance.addLayer(this.marker);
  }

  setFlags(playas, scaleMult = 1) {
    if (!this.instance) return;

    this.playas = playas;

    for(let flag of this.flags) {
      if (flag) this.instance.removeLayer(flag);
    }
    this.flags = [];
    //flag
    if(!this.playas) return; 
    for (let playa of this.playas) {
      let src = '';
      let scale = 0.5;
      if (playa.flag === 0) {
        if (playa.influxLvl === 0) {
          src = 'assets/svg/bandera-verde-baja.png';
          scale = 0.1;
        } else if (playa.influxLvl == 1) {
          src = 'assets/svg/bandera-verde-media.png';
          scale = 0.1;
        } else if (playa.influxLvl == 2) {
          src = 'assets/svg/bandera-verde-alta.png';
          scale = 0.1;
        } else if (playa.influxLvl == 3) {
          src = 'assets/svg/verde-cerrada.png';
          scale = 0.1;
        } else {
          src = 'assets/svg/verde.png';
          scale = 0.5;
        }
      } else if (playa.flag == 1) {
        if (playa.influxLvl === 0) {
          src = 'assets/svg/bandera-amarilla-baja.png';
          scale = 0.1;
        } else if (playa.influxLvl == 1) {
          src = 'assets/svg/bandera-amarilla-media.png';
          scale = 0.1;
        } else if (playa.influxLvl == 2) {
          src = 'assets/svg/bandera-amarilla-alta.png';
          scale = 0.1;
        } else if (playa.influxLvl == 3) {
          src = 'assets/svg/amarilla-cerrada.png';
          scale = 0.1;
        } else {
          src = 'assets/svg/amarilla.png';
          scale = 0.5;
        }
      } else if (playa.flag == 2) {
        if (playa.influxLvl === 0) {
          src = 'assets/svg/bandera-roja-baja.png';
          scale = 0.1;
        } else if (playa.influxLvl == 1) {
          src = 'assets/svg/bandera-roja-media.png';
          scale = 0.1;
        } else if (playa.influxLvl == 2) {
          src = 'assets/svg/bandera-roja-alta.png';
          scale = 0.1;
        } else if (playa.influxLvl == 3) {
          src = 'assets/svg/roja-cerrada.png'
          scale = 0.1;;
        } else {
          src = 'assets/svg/roja.png';
          scale = 0.5;
        }
      } else {
        if (playa.influxLvl === 0) {
          src = 'assets/svg/bandera-desconocida-baja.png';
          scale = 0.1;
        } else if (playa.influxLvl == 1) {
          src = 'assets/svg/bandera-desconocida-media.png';
          scale = 0.1;
        } else if (playa.influxLvl == 2) {
          src = 'assets/svg/bandera-desconocida-alta.png';
          scale = 0.1;
        } else if (playa.influxLvl == 3) {
          src = 'assets/svg/desconocida-cerrada.png'
          scale = 0.1;;
        } else {
          src = '../../assets/svg/bandera-null.png';
          scale = 0.1;
        }
      }

      let icon = new Feature({
        geometry: new geom.Point(this.transformProj(parseFloat(playa.latitude), parseFloat(playa.longitude))),
        name: playa.name
      });
      let iconStyle = new style.Style({
        image: new style.Icon({
          anchor: [0.1, 0.7],
          scale: scale*scaleMult,
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          src: src,
        })
      });
      icon.setStyle(iconStyle);
      let iconSource = new source.Vector({
        features: [icon]
      });
      let flag = new layer.Vector({
        source: iconSource
      });
      this.instance.addLayer(flag);
      this.flags.push(flag);
    }
  }

  ngAfterViewInit() {
    this.instance.updateSize();
  }

  setCenter(lat: number, lng: number){
    if (!this.view) return;
    this.center = this.transformProj(lat, lng);
    this.view.setCenter(this.center);
  }

  setZoom(zoom: number){
    if (!this.view) return;
    this.view.setZoom(zoom);
  }

  refreshMap(){
    if (this.instance){
      this.instance.updateSize();
      this.instance.renderSync();
    }
  }

  // getChildsRegionsInPoint(lat: number, lng: number){
  //   let result = [];
  //   let factory = new jsts.geom.GeometryFactory();
  //   let gpoint = factory.createPoint(new jsts.geom.Coordinate(lng, lat));//new jsts.geom.Point(new jsts.geom.Coordinate(lng, lat));
  //   result = [...result, ...this.children.filter(child => child.wktGeoms.filter(gitem => gitem.contains(gpoint)).length > 0) .map(child => child.region)];
  //   return result;
  // }

  // getChildsRegionsInPixel(pixel: [number, number]){
  //   let coord = this.instance.getCoordinateFromPixel(pixel);
  //   coord = proj.transform(coord, 'EPSG:32628', 'EPSG:4326');
  //   return this.getChildsRegionsInPoint(coord[1], coord[0]);
  // }

  setNormapLayer(layersNames: string[] = null) {
    this.clearNormapLayers();
  }

  clearNormapLayers() {
    //console.log("clearNormapLayers");
    let arrayLayers = this.instance.getLayers().getArray();
    //console.log(arrayLayers.length);
    if(arrayLayers && arrayLayers.length <= 7 )// la capa normap es la 8
      return;
    //console.log("borra");
    this.instance.removeLayer(arrayLayers[arrayLayers.length-1]);

  }

  setDynamicIcons(value) {
    this.dynIcons = value;
  }

}
