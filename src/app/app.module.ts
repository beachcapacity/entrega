import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { IonicSelectableModule } from 'ionic-selectable';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { HTTP } from '@ionic-native/http';
import { DatePipe } from '@angular/common';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Globalization } from '@ionic-native/globalization';
// import { DocumentViewer } from '@ionic-native/document-viewer';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';

import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { PublicListPage } from '../pages/public-list/public-list';
import { PublicMapPage } from '../pages/public-map/public-map';
import { LoginPage } from '../pages/login/login';
import { LocationPage } from '../pages/location/location';
import { TabsPage } from '../pages/tabs/tabs';
import { IncidentReportsPage } from '../pages/incident-reports/incident-reports';
import { PeopleReportsPage } from '../pages/people-reports/people-reports';
import { FlagReportsPage } from '../pages/flag-reports/flag-reports';
import { FlagFormPage } from '../pages/flag-form/flag-form';
import { PeopleFormPage } from '../pages/people-form/people-form';
import { IncidentFormPage } from '../pages/incident-form/incident-form';
import { IncidentForm2Page } from '../pages/incident-form-2/incident-form-2';
import { AffectedFormPage } from '../pages/affected-form/affected-form';
import { BeachDetailsPage } from '../pages/beach-details/beach-details';
import { CoastalInfoDisplayPage } from '../pages/coastal-info-display/coastal-info-display';
import { UpdatePage } from '../pages/update/update';

import { UserProvider } from '../providers/user-provider';
import { PlayaProvider } from '../providers/playa-provider';
import { ReportsProvider } from '../providers/reports-provider';
import { FormsProvider } from '../providers/forms-provider';
import { LayersProvider } from '../providers/layers';
import { UVProvider } from '../providers/uv-provider';
import { LanguageProvider } from '../providers/language/language';
import { CoastalInfoService } from '../providers/coastal-info-service/coastal-info-service';

import { ComponentsModule } from '../components/components.module';

import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import localeEsExtra from '@angular/common/locales/extra/es';
import localeEn from '@angular/common/locales/en';
import localeEnExtra from '@angular/common/locales/extra/en';
import localeDe from '@angular/common/locales/De';
import localeDeExtra from '@angular/common/locales/extra/de';
import localeFr from '@angular/common/locales/Fr';
import localeFrExtra from '@angular/common/locales/extra/fr';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

registerLocaleData(localeEs, 'es', localeEsExtra);
registerLocaleData(localeEn, 'en', localeEnExtra);
registerLocaleData(localeDe, 'de', localeDeExtra);
registerLocaleData(localeFr, 'fr', localeFrExtra);

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PublicListPage,
    PublicMapPage,
    LoginPage,
    LocationPage,
    TabsPage,
    IncidentReportsPage,
    PeopleReportsPage,
    FlagReportsPage,
    FlagFormPage,
    PeopleFormPage,
    IncidentFormPage,
    IncidentForm2Page,
    AffectedFormPage,
    BeachDetailsPage,
    CoastalInfoDisplayPage,
    UpdatePage
  ],
  imports: [
    HttpModule,
    HttpClientModule,
    BrowserModule,
    IonicSelectableModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp,{tabsHideOnSubPages: true}),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PublicListPage,
    PublicMapPage,
    LoginPage,
    LocationPage,
    TabsPage,
    IncidentReportsPage,
    PeopleReportsPage,
    FlagReportsPage,
    FlagFormPage,
    PeopleFormPage,
    IncidentFormPage,
    IncidentForm2Page,
    AffectedFormPage,
    BeachDetailsPage,
    CoastalInfoDisplayPage,
    UpdatePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    Camera,
    AppVersion,
    Device,
    HTTP,
    LocalNotifications,
    UserProvider,
    PlayaProvider,
    ReportsProvider,
    FormsProvider,
    LayersProvider,
    UVProvider,
    LanguageProvider,
    CoastalInfoService,
    DatePipe,
    InAppBrowser,
    Globalization,
    // DocumentViewer,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
