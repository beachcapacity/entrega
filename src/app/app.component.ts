import { Component } from '@angular/core';
import { Platform, App, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';

import { ReportsProvider } from '../providers/reports-provider';
import { UserProvider } from '../providers/user-provider';
//import { PlayaProvider } from '../providers/playa-provider';
import { LanguageProvider } from '../providers/language/language';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  closingApp: boolean = false;

  constructor(
    private platform: Platform, 
    private statusBar: StatusBar, 
    private splashScreen: SplashScreen,
    private reportsProvider: ReportsProvider,
    private userProvider: UserProvider,
    //private playaProvider: PlayaProvider,
    private app: App,
    private translateService: TranslateService,
    private globalization: Globalization,
    private alertCtrl: AlertController
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      // if(this.platform.is('ios')){
      //   this.statusBar.styleDefault();
      // } 
      // else 
      if (this.platform.is('android')) {
        this.statusBar.backgroundColorByHexString('#253776');
      }

      let languageServiceProvider: LanguageProvider = new LanguageProvider(this.translateService, this.globalization);
      languageServiceProvider.setLanguageFromBrowser();

      this.reportsProvider.loadReports();
      this.userProvider.loadUser();

      this.addBackgroundListeners();

      this.splashScreen.hide();

      this.alertCtrl.create({
        title: '¡AVISO!',
        subTitle: 'Esta aplicación aún está en fase de prueba y los datos pueden no coincidir con la realidad.',
        buttons: ['OK']
      }).present();
    });
  }

  addBackgroundListeners(){
    var onResume = () => {
      console.log("Resume");
      if (this.app.getRootNav().getActive().instance.resumeEvent) {
        this.app.getRootNav().getActive().instance.resumeEvent();
      }
    };
    document.addEventListener('resume', onResume, false);
  }
}

