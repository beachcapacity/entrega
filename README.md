Problema de espacio en blanco al final en Iphone X y Iphone 11:

Esto se debe a un problema en el plugin del wkwebview engine. Para arreglarlo hay que hacer un pequeño workaround y añadir en la línea 100 de /platforms/ios/Infoplayas/Plugins/cordova-plugin-wkwebview-engine/CDVWKWebViewEngine.m lo siguiente:

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 110000
    if (@available(iOS 11.0, *)) {
        [wkWebView.scrollView setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    }
#endif 

Source: https://github.com/xtassin/cordova-plugin-wkwebview-engine/commit/97f90b0116e0de5a1ff90d8176faa1684fad3c2a