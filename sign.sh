#!/bin/bash
echo "Realizando build..."
ionic cordova build  android --release --prod --aot

echo "Firmando la aplicación..."
# Info (Keystore generation): keytool -genkey -v -keystore infoplayas.keystore -alias infoplayas -keyalg RSA -keysize 2048 -validity 10000 -noprompt -dname "CN=gestion.infoplayascanarias.es, OU=ID, O=LPAOFFICE, L=Las Palmas de Gran Canaria , S=Las Palmas, C=ES"
# Info (Warning: El almacén de claves JKS utiliza un formato propietario. Se recomienda migrar a PKCS12, que es un formato estándar del sector): keytool -importkeystore -srckeystore infoplayas.keystore -destkeystore infoplayas.keystore -deststoretype pkcs12
jarsigner -sigalg SHA1withRSA -digestalg SHA1 -keystore ./deploy/store/infoplayas.keystore -storepass 'cocoloco' ./platforms/android/build/outputs/apk/android-release-unsigned.apk "infoplayas"

echo "Generando apk final..."
zipalign -f 4  ./platforms/android/build/outputs/apk/android-release-unsigned.apk ./platforms/android/build/outputs/apk/final.apk

rm ./platforms/android/build/outputs/apk/android-release-unsigned.apk
open ./platforms/android/build/outputs/apk/

